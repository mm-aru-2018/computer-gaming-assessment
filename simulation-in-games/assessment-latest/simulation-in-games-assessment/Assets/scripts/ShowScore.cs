﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowScore : MonoBehaviour {
    Text text;
    Scoring scoring;
    int score;
    // DeathZoneCode deathZoneCode;

    // Use this for initialization
    void Start () {
        text = GetComponent<Text>();
        scoring = GameObject.Find("GameScoring").GetComponent<Scoring>();
        // deathZoneCode = GameObject.Find("DeathZone").GetComponent<DeathZoneCode>();
	}
	
	// Update is called once per frame
	void Update () {
        score = (int)(scoring.score);

        text.text = ("Score: " + score.ToString());
	}
}
