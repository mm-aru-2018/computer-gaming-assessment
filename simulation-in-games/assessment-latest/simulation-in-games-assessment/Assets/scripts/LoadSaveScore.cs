﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using System.Runtime.Serialization.Formatters.Binary; // write binary file that can't be modified by player
using System.IO; // file management for C#

public class LoadSaveScore : MonoBehaviour {
    public static LoadSaveScore lss;
    public FinalScore scoring;
    
    public Scene currentScene;
    public int highScore;

    private void Awake()
    {
        lss = this;
        DontDestroyOnLoad(gameObject);
        Load(); // load up score from save file
        // load game
    }

    // Use this for initialization
    void Start () {

	}

	// Update is called once per frame
	void Update () {
        currentScene = SceneManager.GetActiveScene();
        if (scoring == null && currentScene.name == "base")
        {
            scoring = GameObject.Find("FinalPlatform").GetComponent<FinalScore>();
        }
        if (scoring != null && currentScene.name == "base")
        {
            // scoring.score = highScore;
            // highScore = (int)scoring.score;
            if (scoring.platformTouched && highScore < scoring.score)
            {
                highScore = (int)scoring.score;
                Save();
            }
            // Save();
            /*if (scoring.platformTouched && highScore < scoring.hiScore)
            {

            } else
            {
                scoring.hiScore = highScore;
            }*/
        }

	}

    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {

            // for deserialization
            BinaryFormatter bf = new BinaryFormatter();
            Debug.Log(Application.persistentDataPath); // print location of save file

            // open file from location
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open); // persistent data path, hidden like e.g. AppData with Windows

            // set data as deserialized contents of opened file
            PlayerData data = (PlayerData)bf.Deserialize(file); // cast from object to PlayerData class
            file.Close();

            // set the current wave number as deserialized value of class above
            highScore = data.hs;
        }
    }

    public void Save()
    {

            // create binary formatter
            BinaryFormatter bf = new BinaryFormatter();
            // Debug.Log(Application.persistentDataPath); // print location of save file

            // create file at Unity3D persistent data path named playerInfo.at
            FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat"); // persistent data path, hidden like e.g. AppData with Windows

            //
            PlayerData data = new PlayerData
            {
                hs = this.highScore
            };

            // write to file and serialize data
            bf.Serialize(file, data); // write serializable data to file
            file.Close();
        Debug.Log("File saved");
    }
}
[System.Serializable]
class PlayerData
{
    public int hs;
}
