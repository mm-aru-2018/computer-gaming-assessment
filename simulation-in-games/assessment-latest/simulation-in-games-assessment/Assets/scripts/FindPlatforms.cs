﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindPlatforms : MonoBehaviour {
    GameObject[] platforms;
    public GameObject spawnArea;
    // https://answers.unity.com/questions/23620/how-to-change-a-gameobjects-material-via-scripting.html
    // var texture;
    // Use this for initialization
    void Start () {
        // Color noFrict = new Color();
        // https://answers.unity.com/questions/1017582/hex-colors-in-unity.html
        // ColorUtility.TryParseHexString("#00ABFF85", out noFrict);
        spawnArea = GameObject.Find("SpawnArea");
        platforms = GameObject.FindGameObjectsWithTag("Environment");
        for (int i = 0; i < platforms.Length; i++)
        {
            // https://answers.unity.com/questions/1211937/change-color-in-c-with-rgb-values.html
            
            // change color of platforms
            if (platforms[i].GetComponent<Renderer>() != null && platforms[i].name != "FinalPlatform")
            {
                platforms[i].GetComponent<Renderer>().material.color = new Color32(0, 171, 255, 133);
            }

            // change color of platforms and add friction
            if (platforms[i].layer == 12)
            {
                print(platforms[i].name);
                platforms[i].AddComponent<RandFriction>();
                platforms[i].GetComponent<Renderer>().material.color = new Color32(255, 0, 0, 133);
                //platform.texture = 
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
