﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePause : MonoBehaviour {

    public ObjectMovement objectMovement; // find player object movement script to disable during pause menu
    public DeathZoneCode deathZoneCode; // find death zone script to disable pause menu UI when game over
    public GameObject pauseUI; // to toggle pause UI (and maybe game UI?)
    public bool paused;
    public Scoring scoring; // prevent pause menu from being shown when game is complete
        
    private void Awake()
    {
        pauseUI = GameObject.Find("PauseUI");
        deathZoneCode = GameObject.Find("DeathZone").GetComponent<DeathZoneCode>();
        objectMovement = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<ObjectMovement>();
        scoring = GameObject.Find("GameScoring").GetComponent<Scoring>();
        pauseUI.SetActive(false);
        paused = false;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("escape"))
        {
            if (!paused && !deathZoneCode.gameOver && !scoring.objectMovement.gameFinished)
            {
                Cursor.visible = true;
                pauseUI.SetActive(true);
                Cursor.lockState = CursorLockMode.Confined;
                paused = true;
                objectMovement.enabled = false; // disable player movement
                Time.timeScale = 0f;
            } else if (paused && !deathZoneCode.gameOver && !scoring.objectMovement.gameFinished)
            {
                ResumeGame();
            }
        }
	}
    public void ResumeGame()
    {
        Cursor.visible = false;
        pauseUI.SetActive(false);
        Cursor.lockState = CursorLockMode.Confined;
        paused = false;
        objectMovement.enabled = true;
        Time.timeScale = 1f;
    }
}
