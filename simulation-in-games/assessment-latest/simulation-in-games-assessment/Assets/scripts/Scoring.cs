﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scoring : MonoBehaviour {
    
    public ObjectMovement objectMovement;
    public GameObject lss;
    public GameObject lssPrefab;
    public LoadSaveScore lssComponent;

    DeathZoneCode deathZoneCode;
    public FinalScore finalScore;
    public float score, scoreCap;
    
    public bool scoreIncremented;
    public GameObject gameComplete;
    
    private void Awake()
    {
        lss = GameObject.FindGameObjectWithTag("lss");

        // create prefab and get score
        if (lss == null)
        {
            lss = Object.Instantiate(GetPrefab(), Vector3.zero, new Quaternion(0, 0, 0, 0));
        } 
        if (lss != null)
        {
            lssComponent = lss.GetComponent<LoadSaveScore>();
        }
    }

    // Use this for initialization
    void Start () {
        finalScore = GameObject.Find("FinalPlatform").GetComponent<FinalScore>();
        objectMovement = GameObject.Find("Cylinder").GetComponent<ObjectMovement>();
        deathZoneCode = GameObject.Find("DeathZone").GetComponent<DeathZoneCode>();
        gameComplete = GameObject.Find("GameComplete");
        gameComplete.SetActive(false);
        Cursor.visible = false;

        score = 1000f;
        scoreCap = 60f;

	}
	GameObject GetPrefab()
    {
        return lssPrefab;
    }
	// Update is called once per frame
	void Update () {

        //    IncrementScore();
        DecrementScore();
	}
    // if game finished, do this?
    // check final score by counting how many platforms have been touched i.e. code disabled

    // move this to ObjectMovement script?

    /*
    void IncrementScore()
    {
        for (int i = 0; i < platforms.Length; i++)
        {
            score += platforms[i].GetComponent<PlatformTouched>().score;
        }
    }
    */ 

    void DecrementScore()
    {

        if (!objectMovement.gameFinished)
        {
            if (score > scoreCap)
            {
                score -= Time.deltaTime;
            }
            else if (score <= scoreCap)
            {
                score = scoreCap;
            }
        } else if (objectMovement.gameFinished)
        {
            score -= 0;
            finalScore.score = (int) score;
            gameComplete.SetActive(true);
            Cursor.visible = true;
            
        }
        // stop decrementing score when on final platform
        if (deathZoneCode.playerDead)
        {
            if (score > 60)
            {
                score -= 20;
            }
            scoreCap -= 20;
        }
        if (deathZoneCode.gameOver)
        {
            score = 0;
            scoreCap = 0;
        }
    }
}

