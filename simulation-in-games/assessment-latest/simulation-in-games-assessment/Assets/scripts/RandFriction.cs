﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandFriction : MonoBehaviour {
    public float randFriction;
	// Use this for initialization
	void Start () {
        randFriction = Random.Range(0.2f, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
		if (randFriction < 0f)
        {
            randFriction = 0f;
        }
	}
}
