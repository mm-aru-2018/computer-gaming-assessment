﻿ // required to find game objects with tag
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMovement : MonoBehaviour {
    GameObject cylinder;
    DeathZoneCode deathZoneCode;
    public GameObject pellet;
    // BoxCollider collision;

    // publicly adjustable variables for movement
    public float accel = 7f;
    public float maxSpeed = 5f;

    public float angle;
    // variables for rotation
    float minAngle = 0.0f;
    float maxAngle = 90.0f;

    // variables for jumping
    float gravity = -9.8f;
    public float jumpforce = 100f;
    public float frictionCoef;
    public bool onGround = false;
    public PlatformTouched platformTouched;
    public Vector3 velocity, frictionForce;
    public Vector3 normal, weight;
    public LerpPlatformZ lerpZ;
    public LerpPlatformX lerpX;
    public bool onMovingPlatform;
    public float rotateAngle;
    public bool gameFinished;

    // Use this for initialization
    void Start () {
        deathZoneCode = GameObject.Find("DeathZone").GetComponent<DeathZoneCode>();
        cylinder = GameObject.Find("Cylinder"); // https://docs.unity3d.com/ScriptReference/GameObject.FindGameObjectsWithTag.html
        print(cylinder);
        onMovingPlatform = false;
        gameFinished = false;
        // c = GetComponent<BoxCollider>();
        // this.transform.localEulerAngles = new Vector3(0, 0, 90);
        // c = GetComponentInChildren<BoxCollider>();
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (cylinder != null)
        {
            cylinder.transform.position += velocity * Time.deltaTime; // move object based on calculations performed by function below
            cylinder.GetComponent<Rigidbody>().velocity = velocity;
            MoveObject(); // calculate movement based on user input
            Jump();
            ShootPellets();
        } else
        {
            cylinder = GameObject.Find("Cylinder"); // https://docs.unity3d.com/ScriptReference/GameObject.FindGameObjectsWithTag.html
            cylinder.transform.position += velocity * Time.deltaTime; // move object based on calculations performed by function below
            cylinder.GetComponent<Rigidbody>().velocity = velocity;
            MoveObject(); // calculate movement based on user input
            Jump();
        }
    }

    void Jump() // allows player to jump and implements gravity accordingly
    {
        // jump
        if (!onGround)
        {
            velocity.y += (gravity * Time.deltaTime);
        }

        if (onGround && Input.GetKeyDown("space"))
        {
            onGround = false;
            velocity += Vector3.up * jumpforce * Time.deltaTime;
        }
    }

    GameObject GetPellet()
    {
        return pellet;
    }

    void MoveObject()
    {

        angle = Mathf.LerpAngle(minAngle, maxAngle, Time.time); // for smooth rotation of the cylinder

        // move the cylinder
        if (Input.GetKey("w"))
        {
            this.transform.localEulerAngles = new Vector3(0, 0, 0); // rotate ship to face this way https://docs.unity3d.com/ScriptReference/Mathf.LerpAngle.html
            velocity += Vector3.forward * accel * Time.deltaTime;
            if (velocity.z > maxSpeed) { velocity.z = maxSpeed; } // "enforce" a speed limit
            rotateAngle = 0;
        }
        if (Input.GetKey("s"))
        {
            this.transform.localEulerAngles = new Vector3(-2f * angle, 0, 0); // rotate ship to face this way https://docs.unity3d.com/ScriptReference/Mathf.LerpAngle.html
            velocity -= Vector3.forward * accel * Time.deltaTime;
            if (Mathf.Abs(velocity.z) > Mathf.Abs(maxSpeed)) { velocity.z = -maxSpeed; } // "enforce" a speed limit
            rotateAngle = -180;
        }
        if (Input.GetKey("d"))
        {
            this.transform.localEulerAngles = new Vector3(angle, 0, 0); // rotate ship to face this way https://docs.unity3d.com/ScriptReference/Mathf.LerpAngle.html
            velocity += Vector3.right * accel * Time.deltaTime;
            if (velocity.x > maxSpeed) { velocity.x = maxSpeed; } // "enforce" a speed limit
            rotateAngle = 90;
        }
        if (Input.GetKey("a"))
        {
            this.transform.localEulerAngles = new Vector3(-angle, 0, 0); // rotate ship to face this way https://docs.unity3d.com/ScriptReference/Mathf.LerpAngle.html
            velocity -= Vector3.right * accel * Time.deltaTime;
            if (Mathf.Abs(velocity.x) > Mathf.Abs(maxSpeed)) { velocity.x = -maxSpeed; } // "enforce" a speed limit
            rotateAngle = -90;
        }

        // diagonal rotation
        
        if (Input.GetKey("w") && Input.GetKey("d"))
        {
            this.transform.localEulerAngles = new Vector3(0.5f * angle, 0,0);
            rotateAngle = 45;
        }

        if (Input.GetKey("w") && Input.GetKey("a"))
        {
            this.transform.localEulerAngles = new Vector3(-0.5f* angle, 0, 0);
            rotateAngle = -45;
        }

        if (Input.GetKey("s") && Input.GetKey("d"))
        {
            this.transform.localEulerAngles = new Vector3(1.5f* angle, 0, 0);
            rotateAngle = 135;
        }

        if (Input.GetKey("s") && Input.GetKey("a"))
        {
            this.transform.localEulerAngles = new Vector3(-1.5f*angle, 0, 0);
            rotateAngle = -135;
        }
        

        // simulate inertia
        if (!Input.anyKey)
        {
            if (velocity.x > 0.1f || velocity.x < -0.1f || velocity.z > 0.1f || velocity.z < -0.1f) // if object is moving
            {
                // without normalization, object slows down too quickly
                velocity -= velocity.normalized * (accel * Time.deltaTime);
            } else
            {
                // only set velocity of x and z, or else gravity will be affected
                velocity.x = 0; velocity.z = 0;
            }
        }
        if (onMovingPlatform)
        {
            MoveSideways(lerpZ, lerpX);
            // jumpforce = 250f;
            Jump();
        } else if (!onMovingPlatform)
        {
         //    jumpforce = 100f;
        }
    }


    // SHOOT PELLETS
    void ShootPellets()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            // spawn pellet in front of player
            GameObject newPellet = Object.Instantiate(GetPellet(), (this.gameObject.transform.position + (this.gameObject.transform.forward)), transform.rotation) as GameObject;
            newPellet.AddComponent<Projectile>();
            Projectile pro = newPellet.GetComponent<Projectile>();

            // fire according to direction cylinder is facing
            pro.Initialise(CalculateInitialVel(rotateAngle));
            // newPellet.transform.position = newPellet.transform.forward * (float)Mathf.Cos(360 * ((float)Mathf.PI) / 180)*10;
            Destroy(newPellet, 1.5f);
        }
    }
    public float AngleRadians;
    Vector3 CalculateInitialVel(float tempAngle)
    {
        Vector3 tempVel = Vector3.zero;
        AngleRadians = tempAngle * ((float)Mathf.PI)/180;
        if (rotateAngle == 90 || rotateAngle == -90)
        {
            tempVel.y = (float)Mathf.Sin(0) * 7.5F;
            tempVel.x = (float)Mathf.Sin(AngleRadians) * 7.5F;

        } else if (rotateAngle == 0 || rotateAngle == -180)
        {
            tempVel.z = (float)Mathf.Cos(AngleRadians) * 7.5F;
            tempVel.y = (float)Mathf.Sin(0) * 7.5F;
            
        } else if (rotateAngle == 45 || rotateAngle == -135)
        {
            tempVel.y = (float)Mathf.Sin(0) * 7.5F;
            tempVel.x = (float)Mathf.Sin(AngleRadians) * 7.5F;
            tempVel.z = (float)Mathf.Sin(AngleRadians) * 7.5F;
        }
        else if (rotateAngle == -45 || rotateAngle == 135)
        {
            tempVel.y = (float)Mathf.Sin(0) *7.5F;
            tempVel.x = (float)Mathf.Sin(AngleRadians) * 7.5F;
            tempVel.z = -(float)Mathf.Sin(AngleRadians) * 7.5F;
        }
        return tempVel;
    }

    void MoveSideways(LerpPlatformZ a, LerpPlatformX b)
    {
        if (a != null && b == null)
        {
            velocity.z = a.velocity.z * 0.5f;


            if (Input.GetKey("w"))
            {
                velocity += Vector3.forward * accel * 4 * Time.deltaTime;
                if (velocity.z > maxSpeed) { velocity.z = maxSpeed; } // "enforce" a speed limit
            }
            if (Input.GetKey("s"))
            {
                velocity -= Vector3.forward * accel * 4 * Time.deltaTime;
                if (Mathf.Abs(velocity.z) > Mathf.Abs(maxSpeed)) { velocity.z = -maxSpeed; } // "enforce" a speed limit
            }
            if (Input.GetKey("d"))
            {
                velocity += Vector3.right * accel * 2 * Time.deltaTime;
                if (velocity.x > maxSpeed) { velocity.x = maxSpeed; } // "enforce" a speed limit

            }
            if (Input.GetKey("a"))
            {
                velocity -= Vector3.right * accel * 2 * Time.deltaTime;
                if (Mathf.Abs(velocity.x) > Mathf.Abs(maxSpeed)) { velocity.x = -maxSpeed; } // "enforce" a speed limit
            }
        }
        if (b != null && a == null)
        {
            velocity.x = b.velocity.x * 0.5f;


            if (Input.GetKey("w"))
            {
                velocity += Vector3.forward * accel * 2 * Time.deltaTime;
                if (velocity.z > maxSpeed) { velocity.z = maxSpeed; } // "enforce" a speed limit
            }
            if (Input.GetKey("s"))
            {
                velocity -= Vector3.forward * accel * 2* Time.deltaTime;
                if (Mathf.Abs(velocity.z) > Mathf.Abs(maxSpeed)) { velocity.z = -maxSpeed; } // "enforce" a speed limit
            }
            if (Input.GetKey("d"))
            {
                velocity += Vector3.right * accel * 4 * Time.deltaTime;
                if (velocity.x > maxSpeed) { velocity.x = maxSpeed; } // "enforce" a speed limit

            }
            if (Input.GetKey("a"))
            {
                velocity -= Vector3.right * accel * 4 * Time.deltaTime;
                if (Mathf.Abs(velocity.x) > Mathf.Abs(maxSpeed)) { velocity.x = -maxSpeed; } // "enforce" a speed limit
            }
        }


    }
    // ground collision
    // https://answers.unity.com/questions/633480/how-to-detect-child-object-collisions-on-parent.html
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 8 || collision.gameObject.layer == 10 || collision.gameObject.layer == 12)
        {
            platformTouched = collision.gameObject.GetComponent<PlatformTouched>();
            if (platformTouched != null)
            {
                platformTouched.score = 1;
            }
            onGround = true;
            velocity.y = 0;
        } if (collision.gameObject.layer == 10)
        {
            lerpZ = collision.gameObject.GetComponent<LerpPlatformZ>();
            lerpX = collision.gameObject.GetComponent<LerpPlatformX>();
            onMovingPlatform = true;
            // MoveSideways(lerpZ);
        }
        if (collision.gameObject.layer == 12)
        {
            jumpforce = 50f;
        } else if (collision.gameObject.layer == 8)
        {
            jumpforce = 250f;
        }

        if (collision.gameObject.name == "FinalPlatform") // if reach last platform
        {
            gameFinished = true;
            this.enabled = false;
        }

        // increment lives

    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.layer == 12)
        {
            /*
            weight = new Vector3(0, -9.8f, 0);
            normal = -weight;
            frictionCoef = 2f / normal.y;
            frictionForce = (normal * frictionCoef).magnitude * -velocity.normalized;
            velocity += frictionForce;*/
            // this.GetComponent<Rigidbody>().velocity -= this.GetComponent<Rigidbody>().velocity * 0.1f;

            // simulate friction - random friction coefficient
            // decrement speed by coefficient
            velocity -= this.GetComponent<Rigidbody>().velocity * collision.gameObject.GetComponent<RandFriction>().randFriction;

            // disable jumping
            //onGround = false;
            
            // make sure the player doesn't fall through the platform by accumulating gravity
            //gravity = 0.0f;
        }
        if (collision.gameObject.tag == "Environment")
        {
            onGround = true;
        }
        if (collision.gameObject.GetComponent<RandFriction>() == null)
        {
            jumpforce = 250f; // allow plaer to jump high again while on platform if friction removed
        }
    }


    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        { onGround = false;
            jumpforce = 250f;
            
        }
        if (collision.gameObject.layer == 10)
        {
            onMovingPlatform = false;
            jumpforce = 250f;
            
        }
        if (collision.gameObject.layer == 12)
        {/*
            weight = new Vector3(0, -9.8f, 0);
            normal = -weight;
            frictionCoef = 1 / normal.y;
            frictionForce = (normal * frictionCoef).magnitude * -velocity.normalized;
            velocity += gravity * Time.deltaTime;*/
            // onGround = true;

            // reenable jumping
            onGround = false;

            // reenable falling
            

            jumpforce = 250f;
        }
        if (collision.gameObject.tag == "Environment")
        {
            onGround = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Token")
        {
            deathZoneCode.lives++;
            Destroy(other.gameObject);
        }
    }
}
