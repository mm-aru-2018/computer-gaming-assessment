﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinToken : MonoBehaviour {
    float timer;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        
        this.gameObject.transform.localEulerAngles = new Vector3(0, timer * 90, -90);
	}
}
