﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalScore : MonoBehaviour {
    // score will reset when player retries game, have to save score in a file
    Scoring scoring;
    public bool platformTouched;
    public int score;
    // Use this for initialization
    void Start () {
        scoring = GameObject.Find("GameScoring").GetComponent<Scoring>();
        platformTouched = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            platformTouched = true;
            // LoadSaveScore.lss.Save();
        }
    }
}
