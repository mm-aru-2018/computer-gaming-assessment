﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
    public Vector3 velocity, initialVel;
    float gravity = -9.8f;
    float lifetime = 0;
	// Use this for initialization
	void Start () {
		    
	}
	public void Initialise(Vector3 tempInitialVel)
    {
        initialVel = tempInitialVel;
    }
    void ProjectileMove(Vector3 tempVel)
    {
        lifetime += Time.deltaTime;
        velocity.y = (tempVel.y + (gravity * Mathf.Pow(lifetime, 2)) / 2);
        velocity.x = tempVel.x;
        velocity.z = tempVel.z;
        this.transform.position += velocity * Time.deltaTime;

    }
	// Update is called once per frame
	void Update () {
        ProjectileMove(initialVel);
	}
}
