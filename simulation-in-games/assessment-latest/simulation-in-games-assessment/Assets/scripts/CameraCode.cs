﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCode : MonoBehaviour {
    GameObject cylinder;
    Vector3 vec;
	// Use this for initialization
	void Start () {
        cylinder = GameObject.Find("Cylinder"); // find cylinder - child of game object Player pre-rotate
        vec = cylinder.transform.position - this.transform.position; // vector between cylinder and camera
    }
	
	// Update is called once per frame
	void LateUpdate () {
        if (cylinder != null)
        {
            this.transform.position = cylinder.transform.position - vec; // follow cylinder
        } else
        {
            this.transform.position = new Vector3(2.5f,5f,-2.5f);
        }
	}
}
