﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonShoot : MonoBehaviour
{
    Vector3 velocity;
    Vector3 initialVel;
    float gravity = -9.8f;
    float lifeTime = 0;

    // Use this for initialization
    void Start ()
    {
		
	}

    public void Initialise(Vector3 tempInitialVel)
    {
        initialVel = tempInitialVel;
    }

    // Update is called once per frame
    void Update ()
    {
        ProjectileMove(initialVel);
	}

    void ProjectileMove(Vector3 tempVel)
    {
        lifeTime += Time.deltaTime;
        velocity.y = (tempVel.y + (gravity * Mathf.Pow(lifeTime, 2)) / 2);
        velocity.x = tempVel.x;
        velocity.z = tempVel.z;
        this.transform.position += velocity * Time.deltaTime;
    }
}
