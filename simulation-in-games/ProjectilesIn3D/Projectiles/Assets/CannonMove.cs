﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonMove : MonoBehaviour
{
    float shootPower = 10;
    float angleY = 0;
    float angleX = 0;

    // Use this for initialization
    void Start ()
    {
	}
	
	// Update is called once per frame
	void Update ()
    {
        
        RotateCannon(); // Call the Rotate() function
        ChangeShootPower(); // Call the CahngeShootPower() function
        Shooting(); // Call the Shooting() function
        DrawVectors(); //Call the DrawVectors() function
    }

    void RotateCannon() // Change value of angleX and angleY with user input.
    {
       
        if (Input.GetKey(KeyCode.W)) { angleY++; } // If user press W, add 1 unit to the value of angleY
        if (Input.GetKey(KeyCode.S)) { angleY--; } // If user press S, substract 1 unit to the value of angleY
        if (Input.GetKey(KeyCode.A)) { angleX++; } // If user press A, add 1 unit to the value of angleX
        if (Input.GetKey(KeyCode.D)) { angleX--; } // If user press D, substract 1 unit to the value of angleY
        this.transform.localEulerAngles = new Vector3(angleX, angleY, this.transform.localEulerAngles.z); // Change rotation of the cannon base on value of angleX and angleY
    }


    //Function to change the strength of the shoot power
    void ChangeShootPower()
    {
        if (Input.GetKey(KeyCode.E)) { shootPower++; } // If user press K, add 1 unit to the value of angleY
        if (Input.GetKey(KeyCode.Q)) { shootPower--; } // If user press L, substract 1 unit to the value of angleY
        // Cap shoot power to a maximun value
        if (shootPower > 20) { shootPower = 20; } // 
        if (shootPower < 5) { shootPower = 5; }

    }

    //Function to perform the shooting action
    void Shooting()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.position = this.transform.position;
            cube.AddComponent<CannonShoot>();
            CannonShoot pro = cube.GetComponent<CannonShoot>();
            pro.Initialise(CalculateInitial3DVel());
        }
    }

    //Function to calculate the initial velocity of the projectiles in 3D (Using unity's trasnform.forward)
    Vector3 CalculateInitial3DVel()
    {
        Vector3 tempVel = Vector3.zero;
        tempVel = this.transform.forward * shootPower;
        return tempVel;
    }

    //Function to calculate the initial velocity of the projectiles in 2D (Using a mathematical approach)
    Vector3 CalculateInitial2DVel(float tempAngle)
    {
        Vector3 tempVel = Vector3.zero;
        float AngleRadians = tempAngle * ((float)Mathf.PI) / 180;  // /Converts degrees to radians.
        tempVel.x = (float)Mathf.Cos(AngleRadians) * shootPower;
       tempVel.y = (float)Mathf.Sin(AngleRadians) * shootPower;

        return tempVel;
    }

 

    void DrawVectors()
    {
        Debug.DrawRay(transform.position, transform.forward*20,Color.white);
        Debug.DrawRay(transform.position, transform.right*20,Color.red);
    }



}
