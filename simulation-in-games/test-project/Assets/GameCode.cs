﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCode : MonoBehaviour {
    GameObject plane, sphere;
	// Use this for initialization
	void Start () {
        plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
        plane.transform.position = new Vector3(0, 0, 0);
        sphere = GameObject.CreatePrimitive(PrimitiveType.Cube);
        sphere.transform.position = new Vector3(0, 0.5f, 0);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey("w"))
        {
            sphere.transform.Translate(0, 0, 1);
            sphere.transform.Rotate(0, Mathf.Cos(1), 0);
        }
	}
}
