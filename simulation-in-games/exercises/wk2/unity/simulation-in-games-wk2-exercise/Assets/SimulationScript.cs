﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulationScript : MonoBehaviour {

    GameObject cubea, cubeb;
    Vector3 A = Vector3.zero;
    Vector3 origin, dest, dirvect;
    
    public float speed = 5.0f;
    public float N = 5.0f;
    
    // Use this for initialization
    void Start () {

        // spawn cubes
        cubea = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubeb = GameObject.CreatePrimitive(PrimitiveType.Cube);

        // set name of game objects: https://docs.unity3d.com/ScriptReference/Object-name.html
        cubea.name = "CubeA";
        cubeb.name = "CubeB";

        // spawn cubes at specific coordinates
        cubea.transform.position = RandomCoordinates();
        cubeb.transform.position = RandomCoordinates();

        // vector variables
        dest = cubeb.transform.position;
        origin = cubea.transform.position;
        dirvect = dest - origin; // find the difference or vector between CubeB and CubeA.

    }
	
	// Update is called once per frame
	void Update () {

        // movement
        if (Input.GetKey("space")) {
            cubeb.transform.position += transform.up * speed * Time.deltaTime;
        }
        if (Input.GetKey("a"))
        {
            cubeb.transform.position -= transform.right * speed * Time.deltaTime;
        }
        if (Input.GetKey("left ctrl"))
        {
            cubeb.transform.position -= transform.up * speed * Time.deltaTime;
        }
        if (Input.GetKey("d"))
        {
            cubeb.transform.position += transform.right * speed * Time.deltaTime;
        }
        if (Input.GetKey("w"))
        {
            cubeb.transform.position += transform.forward * speed * Time.deltaTime;
        }
        if (Input.GetKey("s"))
        {
            cubeb.transform.position -= transform.forward * speed * Time.deltaTime;
        }

        A = cubea.transform.position; // upoate A to CubeA's current position
        
        // 
        if ((Mathf.Abs(Distance(cubea.transform.position, cubeb.transform.position))) < N) { // follow CubeB - take CubeA's current position from the vector and do it according to each frame incrementally.

            FollowPlayer();
            
            // chase CubeB from a certain distance after three seconds have passed
            Invoke("ChasePlayer", 3); // https://docs.unity3d.com/ScriptReference/MonoBehaviour.Invoke.html
            

        } else
        {
            // reassign dirvect so CubeA will not yet approach or follow CubeB - value is updated per frame
            dirvect = cubeb.transform.position - cubea.transform.position; // find the difference or vector between CubeB and CubeA. // comment out to keep fixed and to keep a distance between CubeA and CubeB
            // points CubeA's z-forward face to dirvect
            cubea.transform.forward = dirvect;
        }

        
    }

    public float Distance(Vector3 a, Vector3 b)
    {
        // take the delta values from the vector points
        float deltaX = b.x - a.x;
        float deltaY = b.y - a.y;
        float deltaZ = b.z - a.z;

        // square the delta values
        deltaX = Mathf.Pow(deltaX, 2);
        deltaY = Mathf.Pow(deltaY, 2);
        deltaZ = Mathf.Pow(deltaZ, 2);

        // add up the deltas
        float dist = deltaX + deltaY + deltaZ;

        // square root the distance
        dist = Mathf.Sqrt(dist);

        return dist;
    }

    public Vector3 RandomCoordinates()
    {
        // generate random float values for primitives to spawn at
        float randX = Random.Range(-10f, 10f);
        float randY = Random.Range(-10f, 10f);
        float randZ = Random.Range(-10f, 10f);

        return new Vector3(randX, randY, randZ);
    }

    public void ChasePlayer(){
        cubea.transform.position = A; // "detach" position of CubeA so no longer relative to CubeB's position

        // reassign dirvect so CubeA can approach CubeB
        dirvect = cubeb.transform.position - cubea.transform.position; // find the difference or vector between CubeB and CubeA. // comment out to keep fixed and to keep a distance between CubeA and CubeB
        cubea.transform.forward = dirvect; // face CubeB

        // move along vector or else cubea will move to relative version of cubeb's position
        cubea.transform.position += dirvect.normalized * speed * Time.deltaTime; // normalize to allow CubeA to move at constant speed.
    }
    public void FollowPlayer()
    {
        
        dest = cubeb.transform.position;
        // dirvect = cubeb.transform.position - cubea.transform.position; // find the difference or vector between CubeB and CubeA. // comment out to keep fixed and to keep a distance between CubeA and CubeB
        origin = cubea.transform.position;

        cubea.transform.position = dest - dirvect;
    }

}
