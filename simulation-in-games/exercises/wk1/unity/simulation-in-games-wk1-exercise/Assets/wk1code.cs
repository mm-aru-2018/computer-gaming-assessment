﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine; // import libraries required for Unity3D

public class wk1code : MonoBehaviour {
    Vector3 A, B, C; // get position of primitives

    // determines the number of (sphere) primitives the user wants to spawn;
    // public, so its value is editable at the inspector
    public int N;

    // user defined spawn limits from CubeA
    // public float userMinDistX, userMinDistY, userMinDistZ;
    public int M;

    // int minDistX, minDistY, minDistZ;
    Vector3 minDist;

    // values for random coordinates
    float randX, randY, randZ;

    // refers to game objects
    GameObject cube, sphere;
    GameObject CubeB;

    int sphereNum;

    // Use this for initialization
    void Start () {
        cube = GameObject.CreatePrimitive(PrimitiveType.Cube); // create a cube
        cube.transform.position = new Vector3(5, 3, 7); // move cube to this position
        A = cube.transform.position; // get cube's coordinates

        // add cube's coordinates
        print("The minimum distance is " + minDist.x + ", " + minDist.y + ", " + minDist.z);

        // instruct user to modify value of N in Inspector
        print("Type the number of primitives (objects) you want to spawn (in inspector), in the game object \"" + this.name + "\", under N.");
        // https://docs.unity3d.com/ScriptReference/Input.html
        // https://docs.unity3d.com/ScriptReference/Input.GetKeyDown.html
        
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("space"))
        {
            // spawn primitives as long as is equal to number specified in inspector
            for (int i = 0; i < N; i++)
            {

                print(randX + ", " + randY + ", " + randZ);
                B = RandomCoordinates(); // vectorise randomly generated floats from each axis
                print("The distance between A and B is + " + Distance(A, B));

                // absolute values used - may not spawn otherwise, or spawn only to the left or right of CubeA
                // if the distance between cube and coordinates to spawn primitives is smaller than M
                if (Mathf.Abs(Distance(A, B)) < Mathf.Abs(M))
                {
                    // spawn at randomly generated coordinates 
                    sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere); // create a sphere
                    sphere.transform.position = new Vector3(randX, randY, randZ); // spawn spheres at random coordinates
                    sphereNum++; // increase count of number of spheres spawned
                }
            }
            print("There are " + sphereNum + " spheres spawned, " + (N - sphereNum) + " spheres could not be spawned.");
        }
        if (Input.GetKeyDown("w"))
        {
            CubeB = GameObject.CreatePrimitive(PrimitiveType.Cube);
            CubeB.transform.position = RandomCoordinates();
            C = CubeB.transform.position;
            Debug.DrawLine(A, C, Color.red, 100.0f); // add time for ray to be visible in editor mode, or else invisible: https://docs.unity3d.com/ScriptReference/Debug.DrawRay.html
        }
    }

    public float Distance(Vector3 a, Vector3 b)
    {
        // take the delta values from the vector points
        float deltaX = b.x - a.x;
        float deltaY = b.y - a.y;
        float deltaZ = b.z - a.z;

        // square the delta values
        deltaX = Mathf.Pow(deltaX, 2);
        deltaY = Mathf.Pow(deltaY, 2);
        deltaZ = Mathf.Pow(deltaZ, 2);

        // add up the deltas
        float dist = deltaX + deltaY + deltaZ;

        // square root the distance
        dist = Mathf.Sqrt(dist);

        return dist;
    }
    
    public Vector3 RandomCoordinates()
    {
        // generate random float values for primitives to spawn at
        randX = Random.Range(-10f, 10f);
        randY = Random.Range(-10f, 10f);
        randZ = Random.Range(-10f, 10f);

        return new Vector3(randX, randY, randZ);
    }
}
