﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// script deals with creation and simulation of object movements - read from class Properties
public class DynamicObjects : MonoBehaviour {
    float mass;
    float volume;
    public Vector3 velocity;
    Vector3 acceleration, localForce, weight, normal, gravity;

    public List<Vector3> externalForces; // list available foreces
    public Rigidbody rb; // access rigidbodies of coponents of game object
    bool onGround, onWater;

    Vector3 buoyance;
    public string type;
    bool active = false;
    Environment environmentMaster; // access environment variables
    Properties properties; // access custom properties of object
	// Use this for initialization
	void Start () {
        //Initialize(Vector3.zero);
	}
    public void Initialize(Vector3 initialVel)
    {
        active = true;
        CheckRB();

        properties = this.GetComponent<Properties>(); // search for object's properties
        volume = properties.volume;
        mass = properties.mass;
        onGround = false;
        externalForces = new List<Vector3>(); // initialize this list with an empty new list of Vector3 variables
        velocity = initialVel;

        environmentMaster = GameObject.Find("EnvironmentMaster").GetComponent<Environment>(); // store data of world enviromnental variables
        gravity = environmentMaster.gravity;
        weight = mass * gravity; // calculate weight force to be mass of object * world gravity

    }
    void CheckRB()
    {
        if (this.GetComponent<Rigidbody>() == null)
        {
            this.gameObject.AddComponent<Rigidbody>();
            rb = this.GetComponent<Rigidbody>();

            // do not use gravity
            rb.useGravity = false;

            // freeze rotation
            rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
        } else // if the object has a rigidbody component
        {
            rb = this.GetComponent<Rigidbody>();

            // do not use gravity
            rb.useGravity = false;

            // freeze rotation
            rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;

        }
    }
    // Update is called once per frame
    void Update () {
		if (!active) { Initialize(Vector3.zero); }
        AddForces();
        UpdateVelocity();
	}
    void UpdateVelocity()
    {
        localForce = Vector3.zero;
        foreach (var force in externalForces) // go through each Vector3 for externalForces list
        {
            localForce += force; // accumulate above to object's local force

        }
        acceleration = localForce / mass; // a = F/m
        velocity += acceleration * Time.deltaTime;
        rb.velocity = velocity;
    }
    void AddForces() // add or remove forces depending on state of object (on water, on ground, in air)
    {
        if ((!onGround) && (!onWater)) // if in air
        {
            // apply only the weight constraint
            if (!externalForces.Contains(weight)) { externalForces.Add(weight); }
            if (externalForces.Contains(normal)) { externalForces.Remove(normal); }
            if (externalForces.Contains(buoyance)) { externalForces.Remove(buoyance); }
        } else if (onWater) // if on water
        {
            // add everything but normal
            if (!externalForces.Contains(weight)) { externalForces.Add(weight); }
            if (!externalForces.Contains(buoyance)) { externalForces.Add(buoyance); }
        } else if (onGround) // if on ground
        {
            // add weight and normal if they're missing
            if (!externalForces.Contains(weight)) { externalForces.Add(weight); }
            if (!externalForces.Contains(normal)) {
                normal = -weight;
                externalForces.Add(normal);
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {

        print("reach");
            ContactPoint contact = collision.contacts[0];
            Idle_on_Ground(contact); // make object idle
            Vector3 velocity2; // velocity of other object
        float mass1 = 0;
            if (collision.gameObject.GetComponent<Structures>() == null)
        {
            mass1 = collision.gameObject.GetComponent<DynamicObjects>().mass;
        } else if (collision.gameObject.GetComponent<DynamicObjects>() == null)
        {
            mass1 = collision.gameObject.GetComponent<Structures>().mass; // get mass of other game object
        }
            Vector3 finalVel;
            // do not create elastic collision for objects without a rigidbody, e.g. the walls.
            if (collision.gameObject.GetComponent<Rigidbody>() == null) {
            velocity2 = Vector3.zero;
            finalVel = (((mass - (mass1)) / (mass + (mass1))) * velocity) + ((2 * mass1) / (mass + mass1)) * velocity2; // set magnitude of game object
            
            } 

            // set velocity of second game object to be that of its own velocity
            else { velocity2 = collision.gameObject.GetComponent<Rigidbody>().velocity; }

            finalVel = (  ((mass - (mass1)) / (mass + (mass1))) * velocity) + ((2 * mass1) / (mass + mass1)) * velocity2; // set magnitude of game object

            // set velocity of this game object - multiply magnitude and direction
            // v' = 2*(v.n)*n-v
            velocity = velocity  - (2 * (Vector3.Dot(velocity, Vector3.Normalize(contact.normal))) * Vector3.Normalize(contact.normal) - velocity).normalized * finalVel.magnitude;
            //-(2 * (Vector3.Dot(velocity, Vector3.Normalize(contact.normal))) * Vector3.Normalize(contact.normal) - velocity).normalized * finalVel.magnitude;

          
            rb.velocity = velocity;
        

        /*else if (collision.gameObject.GetComponent<Structures>() != null)
        {
            ContactPoint contact = collision.contacts[0];
            Idle_on_Ground(contact); // make object idle
            Vector3 velocity2; // velocity of other object
            float mass1 = collision.gameObject.GetComponent<Structures>().mass; // get mass of other game object

            // do not create elastic collision for objects without a rigidbody, e.g. the walls.
            if (collision.gameObject.GetComponent<Rigidbody>() == null) { velocity2 = Vector3.zero; }

            // set velocity of second game object to be that of its own velocity
            else { velocity2 = collision.gameObject.GetComponent<Rigidbody>().velocity; }

            Vector3 finalVel = (((mass - (mass1)) / (mass + (mass1))) * velocity) + ((2 * mass1) / (mass + mass1)) * velocity2; // set magnitude of game object

            // set velocity of this game object - multiply magnitude and direction
            // v' = 2*(v.n)*n-v
            velocity = -(2 * Vector3.Dot(velocity, Vector3.Normalize(contact.normal))) * Vector3.Normalize(contact.normal) - velocity.normalized * finalVel.magnitude;
            rb.velocity = velocity;
        }*/
    }
    private void OnCollisionExit(Collision collision)
    {
        onGround = false;
        externalForces.Remove(normal); // remove normal
    }
    void Idle_on_Ground(ContactPoint contact) // stop object
    {
        if (contact.normal.y > 0.2) // if little to no slope
        {
            onGround = true;
            if (velocity.magnitude < 1f) // stop object from moving at a slow enough speed
            {
                velocity = Vector3.zero;
                rb.velocity = Vector3.zero;
            }
        }
    }
    //void Idle_on_Water()
    private void OnTriggerStay(Collider col) // if in water
    {
        if (col.gameObject.layer == 4) // if object is in water layer
        {
            onWater = true;
            float density = this.GetComponent<Properties>().density;
            buoyance = -ArchimedesLaw(density);

            // fake energy loss from viscosity of water
            this.GetComponent<Rigidbody>().velocity -= this.GetComponent<Rigidbody>().velocity * 0.1f;
            velocity = this.GetComponent<Rigidbody>().velocity;
        }
        
    }
    Vector3 ArchimedesLaw(float density) // calculate buoyancy with Archimedes law;
    {
        buoyance = (density * volume * gravity)/10;
        return buoyance;
    }
    private void OnTriggerExit(Collider col) // if not in water
    {
        onWater = false;
        externalForces.Remove(buoyance);
        buoyance = Vector3.zero;
    }
}
