﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Environment : MonoBehaviour {
    public Vector3 gravity, windForce;

	// Use this for initialization
	void Start () {
        gravity = new Vector3(0, -9.8f, 0);
        windForce = Random.insideUnitSphere * 5; // create wind with a random direction, of fixed magnitude?
                                                 // https://docs.unity3d.com/ScriptReference/Random-insideUnitSphere.html
    }

    // Update is called once per frame
    void Update () {
		
	}
}
