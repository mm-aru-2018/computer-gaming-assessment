﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LauncherMovement : MonoBehaviour {
    public float accel = 1.5f;
    public GameObject prefab;
    public int launchVel;
	// Use this for initialization
	void Start () {
        launchVel = 10;
      //  print(launchVel);
	}
	
	// Update is called once per frame
	void Update () {

        
		if (Input.GetButtonDown("Fire1"))
        {
            // instantiate cubeprefab; "fire" projectile
            GameObject newCube = Object.Instantiate(GetPrefab(), (this.gameObject.transform.position + transform.up * 0.5f), transform.rotation);
            newCube.GetComponent<DynamicObjects>().Initialize(newCube.transform.up * 10f);
           // newCube.GetComponent<Rigidbody>().velocity = newCube.transform.up * (float)launchVel;
            
        }

        if (Input.GetAxis("Horizontal") > 0)
        {
            this.gameObject.transform.localEulerAngles += new Vector3(0, 0, -30f * Time.deltaTime * accel);
        }
        if (Input.GetAxis("Horizontal") < 0)
        {
            this.gameObject.transform.localEulerAngles += new Vector3(0, 0, 30f * Time.deltaTime * accel);
        }
        if (Input.GetKeyDown("w"))
        {

            launchVel += 10;
            print(launchVel);
            if (launchVel >= 100)
            {
                launchVel = 100;
            }
        
        }
        if (Input.GetKeyDown("s"))
        {

            launchVel -= 10;
            print(launchVel);
            if (launchVel <= 10)
            {
                launchVel = 10;
            }

        }

    }
    GameObject GetPrefab()
    {
        return prefab;
    }
}
;