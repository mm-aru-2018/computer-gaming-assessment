﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialVelocity : MonoBehaviour {
    DynamicObjects dynamicObjects;
	// Use this for initialization
	void Start () {
        //Initialize(Vector3.zero);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Initialize(Vector3 initialVel)
    {

        this.gameObject.AddComponent<DynamicObjects>();
        this.GetComponent<DynamicObjects>().Initialize(initialVel); // get dynamic objects script
    }
}
