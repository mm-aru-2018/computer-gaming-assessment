﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// script holds a randomly assigned mass, volume and density for the game object
public class Properties : MonoBehaviour {
    public float mass, volume, density; // public variables for mass, volume, density

	// Use this for initialization
	void Start () {
        // Initialize(Vector3.zero);
        mass = Random.Range(1, 5); // random mass
        volume = Random.Range(1, 30); // random volume
        density = Random.Range(1, 30); // random density
    }
	
	// Update is called once per frame
	void Update () {
		
	}


}


