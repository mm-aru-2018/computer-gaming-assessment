﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCode : MonoBehaviour
{
    GameObject cube;
    Vector3 vec;

    // Use this for initialization
    void Start()
    {
        cube = GameObject.FindGameObjectWithTag("Player");
        vec = cube.transform.position - this.gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (cube != null)
        {
            this.gameObject.transform.position = cube.transform.position - vec;
        }
        else
        {
            this.gameObject.transform.position = new Vector3(0, 0, 0);
        }
    }
}
