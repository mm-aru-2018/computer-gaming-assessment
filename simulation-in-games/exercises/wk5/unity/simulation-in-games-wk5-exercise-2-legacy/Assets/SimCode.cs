﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimCode : MonoBehaviour
{
    public Vector3 SlopeForce = Vector3.zero;
    public Vector3 InputForce = Vector3.zero;
    public Vector3 NormalForce = Vector3.zero;
    public Vector3 FrictionForce;
    public float mass = 5;
    public Vector3 Acc, localForce, Velocity, weight;
    float acceleration;
    float gravity = -9.8f;
    bool onGround = false;
    Vector3 force;
    Vector3 jumpForce = new Vector3(0, 100, 0);
    float frictionCoef;

    bool sideways;

    LerpX lerpX;

    GameObject playerCube, xPlatform;

    // Use this for initialization
    void Start()
    {
        xPlatform = GameObject.Find("XPlatform");
        acceleration = 5F;
        lerpX = xPlatform.GetComponent<LerpX>();
        sideways = false;
            

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        InputMovement();
        AddForces();
        CapVelocity();
    }
    // add input force
    void InputMovement()
    {
        if (Input.GetKey(KeyCode.A))
        { InputForce += -transform.right * (acceleration); }
        if (Input.GetKey(KeyCode.D))
        { InputForce += transform.right * (acceleration); }
        if (Input.GetKey(KeyCode.W))
        { InputForce += transform.forward * (acceleration); }
        if (Input.GetKey(KeyCode.S))
        { InputForce += -transform.forward * (acceleration); }
        if (!Input.anyKey)
        {
            if ((InputForce.x > 0.1) || (InputForce.x < -0.1))
            { InputForce -= InputForce * (acceleration * Time.deltaTime); }
            else { InputForce.x = 0; }
            if ((InputForce.z > 0.1) || (InputForce.z < -0.1))
            { InputForce -= InputForce * (acceleration * Time.deltaTime); }
            else { InputForce.z = 0; }
        }
        // InputForce = Vector3.zero;
        if ((onGround) && (Input.GetKeyDown("space")))
        {
            onGround = false; InputForce += transform.up * 500;
        }
        if (Input.GetKeyDown("left ctrl")){
            this.gameObject.transform.position = new Vector3(-7, 2, 0);
            Velocity = Vector3.zero;
        }

    }
    void AddForces()
    {
        localForce = Vector3.zero;
        weight = this.mass * gravity * Vector3.up; // multiply mass by gravity and normal
        if (!onGround) { localForce += weight; } // increase force downwards by gravitational constant
        else { localForce += (InputForce + SlopeForce + NormalForce); } // add up local force from input, slope and normal
        Acc = localForce / mass;
        Velocity += (Acc * Time.deltaTime);
        CapVelocity();
        this.GetComponent<Rigidbody>().velocity = Velocity;
        
    }
    void CapVelocity()
    {
        if (Velocity.x > 20) Velocity.x = 20;
        if (Velocity.x < -20) Velocity.x = -20;
        if (localForce.x > 300) localForce.x = 300;
        if (localForce.x < -300) localForce.x = -300;

    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            onGround = true;
            InputForce.y = 0;
            this.GetComponent<Rigidbody>().velocity = Vector3.zero;

        }

    }
    void MoveSideways()
    {
        if (sideways)
        {
            Velocity = new Vector3(Velocity.x, Velocity.y, lerpX.velocity.z * 3f);
            //gameObject.transform.position += new Vector3(0, 0, lerpX.gameObject.transform.position.z * 0.01f);
        }
    }


    float angle360(Vector3 from, Vector3 to)
    {
        float angle = Vector3.Angle(from, to);
        return (Vector3.Angle(Vector3.right, to) > 90f) ? 360f - angle : angle;
    }
    void OnCollisionStay(Collision collision)
    {

        onGround = true;
        if (collision.contacts[0].normal.y > 0)
        {
            float tempAngle1 = angle360(collision.contacts[0].normal, Vector3.right) - 90; // calculate platform angles
            tempAngle1 *= (Mathf.PI / 180); // convert to radians
            if (collision.gameObject.tag == "Friction") // apply friction
            {
                frictionCoef = collision.gameObject.GetComponent<Friction>().frCoef;
            } else
            {
                frictionCoef = 1f;
            }
            SlopeForce.x = gravity * mass * Mathf.Sin(tempAngle1);
            SlopeForce.y = gravity * mass * Mathf.Cos(tempAngle1);

            NormalForce.y = -SlopeForce.y; // calculate normal
            transform.up = collision.contacts[0].normal;

            FrictionForce = frictionCoef * NormalForce;
            SlopeForce.x += SlopeForce.normalized.x * FrictionForce.y;
        }
        if (collision.gameObject.name == "XPlatform")
        {
            Debug.Log("On X Platform");
            sideways = true;
            MoveSideways();
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            onGround = false;
        }
    }


}
