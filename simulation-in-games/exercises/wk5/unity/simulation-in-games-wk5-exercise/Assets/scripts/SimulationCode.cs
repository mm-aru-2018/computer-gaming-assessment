﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulationCode : MonoBehaviour {
    public Vector3 SlopeForce = Vector3.zero;
    public Vector3 InputForce = Vector3.zero;
    public Vector3 NormalForce = Vector3.zero;
    public Vector3 FrictionForce;
    public float mass = 5;
    public Vector3 Acc, localForce, Velocity, weight;
    float acceleration;
    float gravity = -9.8f;
    bool onGround = false;
    Vector3 force;
    Vector3 jumpForce = new Vector3(0, 100, 0);
    float frictionCoef;

    GameObject playerCube;

    // Use this for initialization
    void Start () {
        acceleration = 5F;

    }
	
	// Update is called once per frame
	void Update () {
        InputMovement();
        AddForces();
        CapVelocity();
	}
    void InputMovement()
    {
        if (Input.GetKey(KeyCode.A))
        { InputForce += -transform.forward * (acceleration); }
        if (Input.GetKey(KeyCode.D))
        { InputForce += transform.forward * (acceleration); }
        if (Input.GetKey(KeyCode.W))
        { InputForce += transform.right * (acceleration); }
        if (Input.GetKey(KeyCode.S))
        { InputForce += -transform.right * (acceleration); }
        if (!Input.anyKey)
        {
            if ((InputForce.x > 0.1) || (InputForce.x < -0.1))
            { InputForce -= InputForce * (acceleration * Time.deltaTime); }
            else { InputForce.x = 0; }
            if ((InputForce.z > 0.1) || (InputForce.z < -0.1))
            { InputForce -= InputForce * (acceleration * Time.deltaTime); }
            else { InputForce.z = 0; }
        }
        InputForce = Vector3.zero;
        if ((onGround) && (Input.GetKeyDown(KeyCode.LeftControl)))
        {
            onGround = false; InputForce += transform.up * 2000;
        }

    }
    void AddForces()
    {
        localForce = Vector3.zero;
        weight = this.mass * gravity * Vector3.up;
        if (!onGround) { localForce += weight; }
        else { localForce += (InputForce + SlopeForce + NormalForce); }
        Acc = localForce / mass;
        Velocity += (Acc * Time.deltaTime);
        CapVelocity();
        this.GetComponent<Rigidbody>().velocity = Velocity;

    }
    void CapVelocity()
    {
        if (Velocity.x > 20) Velocity.x = 20;
        if (Velocity.x < -20) Velocity.x = -20;
        if (localForce.x > 300) localForce.x = 300;
        if (localForce.x < -300) localForce.x = -300;

    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            onGround = true;
            InputForce.y = 0;
            this.GetComponent<Rigidbody>().velocity = Vector3.zero;

        }
    }


    float angle360(Vector3 from, Vector3 to)
    {
        float angle = Vector3.Angle(from, to);
        return (Vector3.Angle(Vector3.right, to) > 90f) ? 360f - angle : angle;
    }
    void OnCollisionStay(Collision collision)
    {

        onGround = true;
        if (collision.contacts[0].normal.y > 0)
        {
            float tempAngle1 = angle360(collision.contacts[0].normal, Vector3.right) - 90;
            tempAngle1 *= (Mathf.PI / 180);

            //frictionCoef = collision.gameObject.GetComponent<Friction>().frCoef;

            SlopeForce.x = gravity * mass * Mathf.Sin(tempAngle1);
            SlopeForce.y = gravity * mass * Mathf.Cos(tempAngle1);

            NormalForce.y = -SlopeForce.y;
            transform.up = collision.contacts[0].normal;

            FrictionForce = frictionCoef * NormalForce;
            SlopeForce.x += SlopeForce.normalized.x * FrictionForce.y;
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            onGround = false;
        }
    }


}
