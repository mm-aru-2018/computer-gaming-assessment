﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMovement : MonoBehaviour {

    public float gravity = -9.81f;
    public float maxSpeed = 5f;

    public Vector3 velocity;
    public float accel = 2f;
    public float jumpForce = 1000f;
    public bool onGround, moveSideways;
    Vector3 inputForce, localForce, weight, acc;

    GameObject cube, xPlatform;
    LerpX lerpX;
	// Use this for initialization
	void Start () {
        onGround = false;
        cube = GameObject.Find("PlayerCube");
        xPlatform = GameObject.Find("XMovingPlatform");
        lerpX = xPlatform.GetComponent<LerpX>();
        moveSideways = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        ApplyGravity();
        this.GetComponent<Rigidbody>().velocity = velocity;
        // this.GetComponent<Rigidbody>().mass
        MoveObject();
        this.transform.position += velocity * Time.deltaTime;
        
	}

    void ApplyGravity()
    {
        if (onGround && Input.GetKeyDown("space"))
        {
            onGround = false;
            velocity += transform.up * jumpForce * Time.deltaTime;
        }
        if (!onGround)
        {
           velocity.y += (gravity * Time.deltaTime);
        }
    }

    void MoveObject()
    {
        if (Input.GetAxis("Horizontal") > 0)
        {
            velocity += Vector3.forward * accel * Time.deltaTime;
            if (Mathf.Abs(velocity.z) > Mathf.Abs(maxSpeed))
            {
                velocity.z = maxSpeed;
            }
        }
        if (Input.GetAxis("Horizontal") < 0)
        {
            velocity += Vector3.back * accel * Time.deltaTime;
            if (Mathf.Abs(velocity.z) > Mathf.Abs(maxSpeed))
            {
                velocity.z = -maxSpeed;
            }
        }
        if (Input.GetAxis("Vertical") > 0)
        {
            velocity += Vector3.left * accel * Time.deltaTime;
            if (Mathf.Abs(velocity.x) > Mathf.Abs(maxSpeed))
            {
                velocity.x = -maxSpeed;
            }
        }
        if (Input.GetAxis("Vertical") < 0)
        {
            velocity += Vector3.right * accel * Time.deltaTime;
            if (Mathf.Abs(velocity.x) > Mathf.Abs(maxSpeed))
            {
                velocity.x = maxSpeed;
            }
        }
        if (!Input.anyKey)
        {
            if (velocity.z > 0.1f || velocity.z < -0.1f)
            {
                velocity -= velocity.normalized * (accel * Time.deltaTime);
            } else
            {
                velocity = Vector3.zero;
            }
        }
        MoveSideways();

    }

    

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            onGround = true;
            velocity.y = 0;
        }
        if (collision.gameObject.name == "XMovingPlatform")
        {
            Debug.Log("On X platform");
            // this.gameObject.transform.position.x (lerpX.velocity.x * Time.deltaTime);
            moveSideways = true;
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        { onGround = false; }
        if (collision.gameObject.name == "XMovingPlatform")
        {
            Debug.Log("Leaving X platform");
            // this.gameObject.transform.position.x (lerpX.velocity.x * Time.deltaTime);
            moveSideways = false;
        }
    }
    void MoveSideways()
    {
        if (moveSideways)
        {
            velocity.x = lerpX.velocity.x * 0.5f; // move object by 0.5f because amplitude of LerpX is 1.
            if (Input.GetAxis("Vertical") > 0)
            {
                velocity += Vector3.left * accel  * 2 * Time.deltaTime;
                if (Mathf.Abs(velocity.x) > Mathf.Abs(maxSpeed))
                {
                    velocity.x = -maxSpeed;
                }
            }
            if (Input.GetAxis("Vertical") < 0)
            {
                velocity += Vector3.right * accel *2* Time.deltaTime;
                if (Mathf.Abs(velocity.x) > Mathf.Abs(maxSpeed))
                {
                    velocity.x = maxSpeed;
                }
            }
        }
    }
}
