﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpX : MonoBehaviour {

    float timer;
    public float magnitude = 2f;
    public float speed = 1f;
    public Vector3 velocity;
    public Vector3 equilibriumPoint;
	// Use this for initialization
	void Start () {
        equilibriumPoint = new Vector3(0, 0, 0);
    }

    // https://forum.unity.com/threads/moving-along-a-sine-curve.178281/
    // Update is called once per frame
    void Update () {
        timer += Time.deltaTime;
        //velocity.x = (Mathf.Sin(2 + (Time.time * speed))) * magnitude + 1;
        velocity.x = (Mathf.Cos(Time.time * speed) * magnitude);
        
        this.transform.position += velocity * Time.deltaTime;
	}
}
