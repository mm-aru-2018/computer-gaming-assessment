﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimCode : MonoBehaviour
{
    public Vector3 SlopeForce = Vector3.zero;
    public Vector3 InputForce = Vector3.zero;
    public Vector3 NormalForce = Vector3.zero;
    public Vector3 FrictionForce;
    public float mass = 5;
    public Vector3 Acc, localForce, Velocity, weight;
    float acceleration;
    float gravity = -9.8f;
    bool onGround = false;
    Vector3 force;
    Vector3 jumpForce = new Vector3(0, 100, 0);
    float frictionCoef;

    Vector3 startPosition;

    bool sideways;

    LerpX lerpX;

    GameObject playerCube, xPlatform;

    // Use this for initialization
    void Awake()
    {
        startPosition = this.gameObject.transform.position;
    }


    void Start()
    {
        xPlatform = GameObject.Find("XPlatform");
        acceleration = 5F;
        lerpX = xPlatform.GetComponent<LerpX>();
        sideways = false;
            

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        InputMovement();
        AddForces();
        CapVelocity();
    }
    // add input force
    void InputMovement()
    {
        if (Input.GetKey(KeyCode.A))
        { InputForce += -transform.right * (acceleration); }
        if (Input.GetKey(KeyCode.D))
        { InputForce += transform.right * (acceleration); }
        if (Input.GetKey(KeyCode.W))
        { InputForce += transform.forward * (acceleration); }
        if (Input.GetKey(KeyCode.S))
        { InputForce += -transform.forward * (acceleration); }
        if (!Input.anyKey)
        {
            if ((InputForce.x > 0.1) || (InputForce.x < -0.1))
            { InputForce -= InputForce * (acceleration * Time.deltaTime); }
            else { InputForce.x = 0; }
            if ((InputForce.z > 0.1) || (InputForce.z < -0.1))
            { InputForce -= InputForce * (acceleration * Time.deltaTime); }
            else { InputForce.z = 0; }
        }
        // InputForce = Vector3.zero;
        if ((onGround) && (Input.GetKeyDown("space")))
        {
            onGround = false; InputForce += transform.up * 2000;
        }
        if (Input.GetKeyDown("left ctrl")){
            this.gameObject.transform.position = startPosition;
            Velocity = Vector3.zero;
        }

    }
    void AddForces()
    {
        localForce = Vector3.zero;
        weight = this.mass * gravity * Vector3.up; // multiply mass by gravity and normal
        
        FrictionForce = (NormalForce.magnitude * frictionCoef) * -Velocity.normalized; // Section A:  (NormalForce.magnitude * frictionCoef)  deals with magnitude
        //Section B deals with orientation. Friction is NormalForce*FrictionCoef.
        if (!onGround) { localForce += weight; } // increase force downwards by gravitational constant
        // Normal * frictionCoef. <- Give direction to the force.
      
        else { localForce += (InputForce + SlopeForce + NormalForce + FrictionForce); } // add up local force from input, slope and normal
        //print(Velocity);
        Acc = localForce / mass;
        Velocity += (Acc * Time.deltaTime);
        CapVelocity();
        this.GetComponent<Rigidbody>().velocity = Velocity;
        
    }
    void CapVelocity()
    {
        if (Velocity.x > 20) Velocity.x = 20;
        if (Velocity.x < -20) Velocity.x = -20;
        if (localForce.x > 300) localForce.x = 300;
        if (localForce.x < -300) localForce.x = -300;

    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            onGround = true;
            InputForce.y = 0;
            this.GetComponent<Rigidbody>().velocity = Vector3.zero;

        }

    }
    void MoveSideways()
    {
        if (sideways)
        {
            Velocity = new Vector3(Velocity.x, Velocity.y, lerpX.velocity.z * 2f);
            // if no forces are being applied
            if (Acc == Vector3.zero)
            {
                Velocity = lerpX.velocity;
            }
            
        }
    }


    float angle360(Vector3 from, Vector3 to)
    {
        float angle = Vector3.Angle(from, to);
        return (Vector3.Angle(Vector3.right, to) > 90f) ? 360f - angle : angle;
    }
    void OnCollisionStay(Collision collision)
    {

        onGround = true;
        if (collision.contacts[0].normal.y > 0)
        {
            float tempAngle1 = angle360(collision.contacts[0].normal, Vector3.right) - 90; // calculate platform angles
            tempAngle1 *= (Mathf.PI / 180); // convert to radians - otherwise, forces applied will be excessive
            
            if (collision.gameObject.tag == "Friction") // apply friction on slopes with the component
            {
                frictionCoef = collision.gameObject.GetComponent<Friction>().frCoef;
            } else
            {
                frictionCoef = 0f; // do not apply friction
            }

            // calculate slope force magnitude and direction - take gravity, mass and angle of slope
            SlopeForce.x = gravity * mass * Mathf.Sin(tempAngle1);
            SlopeForce.y = gravity * mass * Mathf.Cos(tempAngle1);

            NormalForce.y = -SlopeForce.y; // calculate normal, from slope force magnitude and direction
            transform.up = collision.contacts[0].normal; // rotate object to be perpendicular to platform it is on
            
            FrictionForce = frictionCoef * NormalForce; // add a counteracting force against player input

            // increment slope force horizontally by friction and direction of slope force
            SlopeForce.x += SlopeForce.normalized.x * FrictionForce.y; 
            
        }
        if (collision.gameObject.name == "XPlatform")
        {
            Debug.Log("On X Platform");
            sideways = true;
            MoveSideways();
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            onGround = false;
        }
    }


}
// SlopeForce.x = -Velocity.normalized.x * (frictionCoef*10); // Fake friction by damping the velocity of the object. 
//gameObject.transform.position += new Vector3(0, 0, lerpX.gameObject.transform.position.z * 0.01f);