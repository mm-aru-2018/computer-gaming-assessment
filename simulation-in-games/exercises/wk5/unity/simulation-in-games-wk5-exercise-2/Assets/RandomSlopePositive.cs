﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSlopePositive : MonoBehaviour
{
    public float slopeAngle;
    // Use this for initialization
    void Start()
    {
        slopeAngle = UnityEngine.Random.Range(-30f, 0f);
        this.gameObject.transform.localEulerAngles = new Vector3(0, 0, slopeAngle);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
