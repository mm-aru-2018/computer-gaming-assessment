﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpY : MonoBehaviour
{
    float timer;
    public float magnitude = 2f;
    public float speed = 1f;
    public Vector3 velocity;
    public Vector3 equilibriumPoint;
    // Use this for initialization
    void Start()
    {
        equilibriumPoint = new Vector3(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        //velocity.y = (Mathf.Sin(2 + (Time.time * speed))) * magnitude + 1;

        // moving along sine will cause the equilibrium point to be equal to one - shifting it will make it go down gradually, even with cosine
        velocity.y = (Mathf.Cos(Time.time * speed) * magnitude);

        this.transform.position += velocity * Time.deltaTime;
    }
}
