﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeCode : MonoBehaviour {

    public float accel = 10f;
    public float maxSpeed = 5f;

    public float gravity = -9.8f;

    bool onGround = false;
    public float jumpforce = 500f;

    Vector3 Velocity;

	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {

        if (!onGround)
        {
            Velocity.y += (gravity * Time.deltaTime);
        }

        if (onGround && Input.GetKeyDown("space"))
        {
            onGround = false;
            Velocity += transform.up * jumpforce * Time.deltaTime;
        }
        this.transform.position += Velocity * Time.deltaTime; // corresponds to displacement = velocity * time.
        this.GetComponent<Rigidbody>().velocity = Velocity;
        Debug.DrawLine(this.transform.position, this.transform.position + Velocity, Color.blue, 0.1f);
        SetVel();
	}

    // set velocity
    void SetVel()
    {
        if (Input.GetKey("w"))
        {
            Velocity += Vector3.forward * accel * Time.deltaTime;
            if (Velocity.y > maxSpeed) { Velocity.y = maxSpeed; }
        }
        if (Input.GetKey("s"))
        {
            Velocity -= Vector3.forward * accel * Time.deltaTime;
            if (Mathf.Abs(Velocity.y) > Mathf.Abs(maxSpeed)) { Velocity.y = -maxSpeed; }
        }
        if (Input.GetKey("a"))
        {
            Velocity -= Vector3.right * accel * Time.deltaTime;
            if (Mathf.Abs(Velocity.x) > Mathf.Abs(maxSpeed)) { Velocity.x = -maxSpeed; }
        }
        if (Input.GetKey("d"))
        {
            Velocity += Vector3.right * accel * Time.deltaTime;
            if (Velocity.x > maxSpeed)
            {
                Velocity.x = maxSpeed;
            }
        }

        if (!Input.anyKey) // if neither of the above key is held down
        { // simulate inertia
            if (Velocity.x > 0.1f || Velocity.x < -0.1f || Velocity.z > 0.1f || Velocity.z < -0.1f) // if object is moving
            {
                Velocity -= Velocity.normalized * (accel * Time.deltaTime);
                // without normalization, object slows down too quickly
                
            }
            else
            {
                Velocity = Vector3.zero;
            }
        }
    }

     void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            onGround = true;
            Velocity.y = 0;
        }          
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        { onGround = false; }      
    }

}
