﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    GameObject cube;
    Vector3 vec;
	// Use this for initialization
	void Start () {
        cube = GameObject.Find("Cube");
        vec = cube.transform.position - this.transform.position; // create vector between cube and camera
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position = cube.transform.position - vec; // follow cube continually
    }
}
