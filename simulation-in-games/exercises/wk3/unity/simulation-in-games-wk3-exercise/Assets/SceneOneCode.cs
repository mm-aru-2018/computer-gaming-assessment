﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneOneCode : MonoBehaviour {
    GameObject cubeA, cubeB, playerCube; // declare game objects
    public float maxSpeedX = 5f; // maximum speed
    float accel = 2f; // acceleration

    float accelA, accelB;
    float speedA, speedB;

    Vector3 velocity; // velocity
    Vector3 velocityA, velocityB;
    
    void Start () {

        // instantiate game objects as cubes to spawn in the scene
        cubeA = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubeB = GameObject.CreatePrimitive(PrimitiveType.Cube);
        playerCube = GameObject.CreatePrimitive(PrimitiveType.Cube);

        // spawn the cubes at a slight left offset
        cubeA.transform.position = new Vector3(-5, 2, 0);
        cubeB.transform.position = new Vector3(-5, 0, 0);
        playerCube.transform.position = new Vector3(-5, -2, 0);

        // randomly instantiate accelerations and maximum speeds
        accelA = Random.Range(0.1f, 1f);
        accelB = Random.Range(0.1f, 1f);
        speedA = Random.Range(0.1f, 5f);
        speedB = Random.Range(0.1f, 5f);

        
    }
	
	// Update is called once per frame
	void Update () {
        // Time.deltaTime for smooth, frame-indepenenden movement: https://answers.unity.com/questions/1231152/how-timedeltatime-affects-movement.html
        // https://forum.unity.com/threads/c-how-to-use-time-deltatime-to-move-object-smoothly.307197/

        velocityA += Vector3.right * accelA * Time.deltaTime;
        velocityB += Vector3.right * accelB * Time.deltaTime;

        if (velocityA.x > speedA) { velocityA.x = speedA; }
        if (velocityB.x > speedB) { velocityB.x = speedB; }

        if (Input.GetKey("d")){
            velocity += Vector3.right * accel * Time.deltaTime; // move by one X times acceleration times time - last value for smooth movement
            if (velocity.x > maxSpeedX) { velocity.x = maxSpeedX;  } // enforce speed limit
            // print(velocity.x);
        }
        if (Input.GetKey("a"))
        {
            velocity -= Vector3.right * accel * Time.deltaTime; // move by minus one X times acceleration times time - last value for smooth movement
            if (Mathf.Abs(velocity.x) > Mathf.Abs(maxSpeedX)) { velocity.x = -maxSpeedX; } // enforce speed limit
            // print(velocity.x);
        }
        if (!Input.anyKey) // if neither of the above key is held down
        { // simulate inertia
            if (velocity.x > 0.1f || velocity.x < -0.1f) // if object is moving
            {
                // without normalization, object slows down too quickly
                velocity -= velocity.normalized * (accel * Time.deltaTime);
            } else
            {
                velocity = Vector3.zero;
            }
        }
        playerCube.transform.position += velocity * Time.deltaTime; // move player, respecting physics rules above, according to time again to slow it down
                                                                    // - physics have to be calculated first before repositioning player
                                                                    // in order to accurately simulate physics

        cubeA.transform.position += velocityA * Time.deltaTime;
        cubeB.transform.position += velocityB * Time.deltaTime;
    }
}
