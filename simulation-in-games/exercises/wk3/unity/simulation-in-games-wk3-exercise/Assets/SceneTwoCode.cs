﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneTwoCode : MonoBehaviour {
    GameObject ship;

    public float accel = 7f;
    public float maxSpeed = 5f;

    // for lerping rotation
    float minAngle = 0.0f;
    float maxAngle = 90.0f;

    Vector3 velocity;
    Vector3 dirvect;

    // Use this for initialization
    void Start () {
        ship = GameObject.CreatePrimitive(PrimitiveType.Capsule);
        ship.transform.position = new Vector3(0, 0, 0); // spawn ship to origin.
	}
	
	// Update is called once per frame
	void Update () {
        // dirvect = velocity - ship.transform.position;
        ship.transform.position += velocity * Time.deltaTime;
        Debug.DrawLine(ship.transform.position, ship.transform.position + velocity, Color.black, 0.1f); // will always draw line here from ship. Shorter duration for no trailing lines
        SetVelRot();
    }

    // set velocity and rotation
    void SetVelRot()
    {
        // smoothly rotate ship to direction of travel
        float angle = Mathf.LerpAngle(minAngle, maxAngle, Time.time);
        if (Input.GetKey("d"))
        {
            ship.transform.eulerAngles = new Vector3(0, 0, -angle); // rotate ship to face this way https://docs.unity3d.com/ScriptReference/Mathf.LerpAngle.html
            velocity += Vector3.right * accel * Time.deltaTime;
            if (velocity.x > maxSpeed) { velocity.x = maxSpeed; }
        }
        if (Input.GetKey("a"))
        {
            ship.transform.eulerAngles = new Vector3(0, 0, angle);
            velocity -= Vector3.right * accel * Time.deltaTime;
            if (Mathf.Abs(velocity.x) > Mathf.Abs(maxSpeed)) { velocity.x = -maxSpeed; }
        }
        if (Input.GetKey("w"))
        {
            ship.transform.eulerAngles = new Vector3(0, 0, 0);
            velocity += Vector3.up * accel * Time.deltaTime;
            if (velocity.y > maxSpeed) { velocity.y = maxSpeed; }
        }
        if (Input.GetKey("s"))
        {
            ship.transform.eulerAngles = new Vector3(0, 0, -2 * angle);
            velocity -= Vector3.up * accel * Time.deltaTime;
            if (Mathf.Abs(velocity.y) > Mathf.Abs(maxSpeed)) { velocity.y = -maxSpeed; }
        }

        // diagonal rotations
        if (Input.GetKey("w") && Input.GetKey("d"))
        {
            ship.transform.eulerAngles = new Vector3(0, 0, -0.5f * angle);
        }

        if (Input.GetKey("w") && Input.GetKey("a"))
        {
            ship.transform.eulerAngles = new Vector3(0, 0, 0.5f * angle);
        }

        if (Input.GetKey("s") && Input.GetKey("d"))
        {
            ship.transform.eulerAngles = new Vector3(0, 0, -1.5f * angle);
        }

        if (Input.GetKey("s") && Input.GetKey("a"))
        {
            ship.transform.eulerAngles = new Vector3(0, 0, 1.5f * angle);
        }

        if (!Input.anyKey) // if neither of the above key is held down
        { // simulate inertia
            if (velocity.x > 0.1f || velocity.x < -0.1f || velocity.y > 0.1f || velocity.y < -0.1f) // if object is moving
            {
                // without normalization, object slows down too quickly
                velocity -= velocity.normalized * (accel * Time.deltaTime);
            }
            else
            {
                velocity = Vector3.zero;
            }
        }
    }

}
