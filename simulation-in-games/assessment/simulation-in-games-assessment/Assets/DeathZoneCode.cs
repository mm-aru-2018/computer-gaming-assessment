﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZoneCode : MonoBehaviour {

    GameObject player;
    bool playerDead = false;

	// Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if (playerDead)
        {
            GameObject.Destroy(player);
        }
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player && player != null)
        {
            playerDead = true;
        }
    }
    void OnTriggerExit(Collider other)
    {

    }
}
