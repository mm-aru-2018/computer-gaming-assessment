﻿using System; // required to find game objects with tag
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMovement : MonoBehaviour {
    GameObject t;
    BoxCollider c;

    // publicly adjustable variables for movement
    public float accel = 7f;
    public float maxSpeed = 5f;

    // variables for rotation
    float minAngle = 0.0f;
    float maxAngle = 90.0f;

    // variables for jumping
    float gravity = -9.8f;
    public float jumpforce = 100f;
    bool onGround = false;

    Vector3 velocity;
    // Use this for initialization
    void Start () {
        // t = FindGameObjectsWithTag("Environment"); // https://docs.unity3d.com/ScriptReference/GameObject.FindGameObjectsWithTag.html
        // c = GetComponent<BoxCollider>();
        // this.transform.eulerAngles = new Vector3(0, 0, 90);
    }

    // Update is called once per frame
    void Update () {
        this.transform.position += velocity * Time.deltaTime; // move object based on calculations performed by function below
        this.GetComponent<Rigidbody>().velocity = velocity;
        MoveObject(); // calculate movement based on user input
        Jump();
    }

    void Jump() // allows player to jump and implements gravity accordingly
    {
        // jump
        if (!onGround)
        {
            velocity.y += (gravity * Time.deltaTime);
        }

        if (onGround && Input.GetKeyDown("space"))
        {
            onGround = false;
            velocity += Vector3.up * jumpforce * Time.deltaTime;
        }
    }

    void MoveObject()
    {

        float angle = Mathf.LerpAngle(minAngle, maxAngle, Time.time); // for smooth rotation of the cylinder

        // move the cylinder
        if (Input.GetKey("w"))
        {
            this.transform.eulerAngles = new Vector3(0, 0, angle); // rotate ship to face this way https://docs.unity3d.com/ScriptReference/Mathf.LerpAngle.html
            velocity += Vector3.forward * accel * Time.deltaTime;
            if (velocity.z > maxSpeed) { velocity.z = maxSpeed; } // "enforce" a speed limit
        }
        if (Input.GetKey("s"))
        {
            this.transform.eulerAngles = new Vector3(0, 0, -angle); // rotate ship to face this way https://docs.unity3d.com/ScriptReference/Mathf.LerpAngle.html
            velocity -= Vector3.forward * accel * Time.deltaTime;
            if (Mathf.Abs(velocity.z) > Mathf.Abs(maxSpeed)) { velocity.z = -maxSpeed; } // "enforce" a speed limit
        }
        if (Input.GetKey("d"))
        {
            this.transform.eulerAngles = new Vector3(angle, 0, 0); // rotate ship to face this way https://docs.unity3d.com/ScriptReference/Mathf.LerpAngle.html
            velocity += Vector3.right * accel * Time.deltaTime;
            if (velocity.x > maxSpeed) { velocity.x = maxSpeed; } // "enforce" a speed limit

        }
        if (Input.GetKey("a"))
        {
            this.transform.eulerAngles = new Vector3(-angle, 0, 0); // rotate ship to face this way https://docs.unity3d.com/ScriptReference/Mathf.LerpAngle.html
            velocity -= Vector3.right * accel * Time.deltaTime;
            if (Mathf.Abs(velocity.x) > Mathf.Abs(maxSpeed)) { velocity.x = -maxSpeed; } // "enforce" a speed limit
        }

        // diagonal rotation
        if (Input.GetKey("w") && Input.GetKey("d"))
        {
            this.transform.eulerAngles = new Vector3(0, 0, 0);
            this.transform.eulerAngles = new Vector3(0.5f * angle, 0, 0);
        }

        if (Input.GetKey("w") && Input.GetKey("a"))
        {
            this.transform.eulerAngles = new Vector3(0, 0, angle);
            this.transform.eulerAngles = new Vector3(-0.5f * angle, 0, 0);
        }

        if (Input.GetKey("s") && Input.GetKey("d"))
        {
            this.transform.eulerAngles = new Vector3(0, 0, -angle);
            this.transform.eulerAngles = new Vector3(0.5f * angle, 0, 0);
        }

        if (Input.GetKey("s") && Input.GetKey("a"))
        {
            this.transform.eulerAngles = new Vector3(0, 0, -angle);
            this.transform.eulerAngles = new Vector3(-0.5f * angle, 0, 0);
        }


        // simulate inertia
        if (!Input.anyKey)
        {
            if (velocity.x > 0.1f || velocity.x < -0.1f || velocity.z > 0.1f || velocity.z < -0.1f) // if object is moving
            {
                // without normalization, object slows down too quickly
                velocity -= velocity.normalized * (accel * Time.deltaTime);
            } else
            {
                velocity = Vector3.zero;
            }
        }

    }

    // ground collision
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            onGround = true;
            velocity.y = 0;
        }

    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        { onGround = false; }
    }
}
