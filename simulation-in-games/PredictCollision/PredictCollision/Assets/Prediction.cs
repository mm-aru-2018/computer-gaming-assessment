﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Prediction : MonoBehaviour {

    public GameObject target;
    Vector3 DirVect;
    Vector3 Dir2;
    Move ObjectData;
    float speed;
    bool ShootActive;
    Vector3[] positions = new Vector3[5];
	// Use this for initialization
	void Start ()
    {
        speed = 10;
        ObjectData = target.GetComponent<Move>();
     }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        CalculateDirVect();
        DrawVector();
        if(ShootActive==false)
        { Predict_Col(); }
        if (Input.GetKey(KeyCode.Space))
        {
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
        }
       
    }

    void CalculateDirVect()
    {
       // DirVect = (target.transform.position - this.transform.position).normalized;
    }
    void DrawVector()
    {
        // Debug.DrawRay(this.transform.position, DirVect, Color.red);
        
    }

    void Predict_Col()
    {
        for (int i = 0; i < positions.Length; i ++)
        {
            // v =  p / t ; p = v*t .  t =  p/v;
            positions[i] = (target.transform.position + (ObjectData.GetComponent<Move>().Vel * i)/3);
            
            float time = (positions[i] - target.transform.position).magnitude / ObjectData.GetComponent<Move>().Vel.magnitude;
            float time1 = (positions[i] - this.transform.position).magnitude / speed;
          /*  print("Bullet will take " + time1 + " seconds");
            print("Car will take "+time+ " seconds");*/
            
            if (Mathf.Abs(time1 -time) < 0.2f)
            {
                print(Mathf.Abs(time1 - time));
                print(positions[i]);
                ShootActive = true;

                Vector3 DirVect1;
                DirVect1 = (positions[i] - this.transform.position).normalized;
                DirVect = DirVect1;
                Dir2 = (positions[i]- target.transform.position).normalized;
                Shoot(DirVect1);
            }
            
        }
    }

    void Shoot(Vector3 DirVect1)
    {
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.transform.tag = "bullet";
        cube.transform.position = this.transform.position;
        cube.AddComponent<Move1>(); cube.GetComponent<Move1>().Instanstiate(DirVect1*speed);
        Debug.DrawRay(this.transform.position , DirVect1*200, Color.red);
        Debug.DrawRay(target.transform.position , Dir2*200, Color.yellow);
    }
}

