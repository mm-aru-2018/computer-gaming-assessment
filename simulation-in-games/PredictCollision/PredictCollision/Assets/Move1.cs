﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move1 : MonoBehaviour {

    public Vector3 Vel;
	// Use this for initialization
	void Start ()
    {
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        this.transform.position += Vel * Time.deltaTime; 
	}

    public void Instanstiate(Vector3 tempVel)
    {
        Vel = tempVel;
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag =="bullet")
        {
            Destroy(col.gameObject);
            Destroy(this.gameObject);
        }
    }
}
