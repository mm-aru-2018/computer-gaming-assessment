﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WaveManagement : MonoBehaviour {
    public int waveNo, enemiesRemaining, killstreak, noEnemies, maxAmmo, playerDmg;
    public EnemySpawn enemySpawn;
    public PlayerSpawn playerSpawn;
    public GameObject UICountdown;
    

    public float attackdmg, ksTimer;
    public bool refillAmmoBonus;
    // Use this for initialization
    private void Awake()
    {
        playerSpawn = GameObject.Find("PSpawnVolume").GetComponent<PlayerSpawn>();
        enemySpawn = GameObject.Find("ESpawnVolume").GetComponent<EnemySpawn>();
        UICountdown = GameObject.Find("Countdown");
    }
    void Start () {
        refillAmmoBonus = false;
        // find enemy spawn volume
        

        // wave number
        waveNo = 1;
        
        killstreak = 0;

        // number of enemies to spawn each wave
        noEnemies = 10;
        enemiesRemaining = noEnemies;

        // set enemy attack damage - increment per wave.
        attackdmg = 0f;

        // set maximum ammo
        maxAmmo = 500;
        // set player damage
        playerDmg = 20;

	}
	
	// Update is called once per frame
	void Update () {
        switch (waveNo)
        {
            case 1:
                noEnemies = 10;
                attackdmg = 1f;
                //enemySpawn._ok = true;
                if (enemiesRemaining == 0) //advance to next wave and respawn enemies
                {
                    waveNo++;
                    enemySpawn._ok = true;
                    enemiesRemaining = noEnemies; // "reset" enemies remaining counter for the player's benefit
                }
                break;
            case 2:
                noEnemies = 10;
                attackdmg = 5f;
                if (enemiesRemaining == 0)
                {
                    waveNo++;
                    enemySpawn._ok = true;
                    enemiesRemaining = noEnemies;
                }
                break;
            case 3:
                noEnemies = 10;
                attackdmg = 3f;
                if (enemiesRemaining == 0)
                {
                    waveNo++;
                    enemySpawn._ok = true;
                    enemiesRemaining = 15;
                }
                break;
            case 4:
                noEnemies = 15;
                attackdmg = 4f;
                if (enemiesRemaining == 0)
                {
                    waveNo++;
                    enemySpawn._ok = true;
                    enemiesRemaining = 15;
                }
                break;
            case 5:
                noEnemies = 15;
                attackdmg = 5f;
                if (enemiesRemaining == 0)
                {
                    break;
                }
                break;
            default:
                noEnemies = 0;
                attackdmg = 0f;
                break;
        }
        UpgradePlayer();
        /*
        if (waveNo == 1)
        {
            if (enemiesRemaining == 0)
            {
                waveNo = 2;
                noEnemies = 10;
            }
        }
        
        if (enemiesRemaining == 0 && waveNo <= 5 && waveNo > 1)
        {
            // reset number of enemies killed for next wave
            enemiesRemaining = noEnemies;

            // increase wave number
            waveNo++;

            // spawn more enemies
            enemySpawn._ok = true;
        } else if (waveNo >= 6)
        {
            // stop incrementing waves when all completed
            noEnemies = 0;
            waveNo += 0;
        }*/

        
	}
    void UpgradePlayer()
    {
        
        if (killstreak >= 7 && playerDmg <100)
        {
            // upgrade player's damage
            playerSpawn.playerMovement.damageModified = false;
            playerDmg += 20;
            print(playerDmg);
            killstreak = 0; 
        }
        
    }
}
