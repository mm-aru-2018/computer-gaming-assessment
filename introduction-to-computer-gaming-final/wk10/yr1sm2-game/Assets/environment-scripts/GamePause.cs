﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePause : MonoBehaviour {

    public FindComponents findComponents;
    public GameObject pauseUI;
    public GameOver gameOver;
    public PlayerSpawn playerSpawn; // access player spawn code to prevent camera from moving while in pause menu
    public bool paused;
    private void Awake()
    {
        // access game UI
        findComponents = GameObject.Find("Game").GetComponent<FindComponents>();
        
        // find pause UI
        pauseUI = GameObject.Find("PauseUI");

        // access code to prevent pause menu from being accessible within game over
        gameOver = GameObject.Find("Game").GetComponent<GameOver>();

        // access player spawn to access player properties, to stop camera rotating within pause menu
        playerSpawn = GameObject.Find("PSpawnVolume").GetComponent<PlayerSpawn>();
        pauseUI.SetActive(false);
        paused = false;
    }

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        
        // if user has pressed the Esc key
		if (Input.GetKeyDown("escape")) 
        {
            // debug - let player know they have invoked pause or resume
            Debug.Log("Game pause menu/resume invoked");

            // if not paused and not game over
            if (!paused && !gameOver.isGameOver)
            {
                Cursor.visible = true;
                findComponents.gameplayUI.SetActive(false);
                pauseUI.SetActive(true);
                Cursor.lockState = CursorLockMode.Confined;
                paused = true;
                playerSpawn.camMouseLook.enabled = false;
                playerSpawn.spawnDefenses.enabled = false;
                playerSpawn.manualMovement.enabled = false;
                Time.timeScale = 0f; // pause game activity
            }

            // if paused and not game over
            else if (paused && !gameOver.isGameOver)
            {
                ResumeGame();
            } else if (gameOver.isGameOver)
            {
                // if game over, disable this game object
                this.gameObject.SetActive(false);
            }
            // pause the game by toggling it
        }
	}

    public void ResumeGame() // function accessable for resume buttton
    {
        Cursor.lockState = CursorLockMode.Locked;
        findComponents.gameplayUI.SetActive(true);
        pauseUI.SetActive(false);
        Cursor.visible = false;

        paused = false;
        playerSpawn.camMouseLook.enabled = true;
        playerSpawn.spawnDefenses.enabled = true;
        playerSpawn.manualMovement.enabled = true;
        Time.timeScale = 1.0f; // resume game activity
    }

}
