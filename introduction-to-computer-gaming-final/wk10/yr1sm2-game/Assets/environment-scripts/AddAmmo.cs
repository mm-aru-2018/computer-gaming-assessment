﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddAmmo : MonoBehaviour {
    PlayerShoot playerShoot;
    public GameObject player;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Destroy(this.gameObject, 30f);
	}
    private void OnTriggerEnter(Collider other)
    {
        // check if other object is player
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Player interacting with health kit");
            player = other.gameObject;
            if (player != null)
            {
                playerShoot = other.GetComponentInChildren<PlayerShoot>();

                // if player's ammo is less than 410 (default values)
                if (playerShoot.ammo < playerShoot.waveManagement.maxAmmo && playerShoot.ammo < playerShoot.waveManagement.maxAmmo - 90)
                {
                    playerShoot.ammo += 90;
                    Destroy(this.gameObject);
                }
                // add remaining ammo if greater than maximum ammo limit
                // if player's ammo is between 410 and including 500 (default values)
                else if (playerShoot.ammo > (playerShoot.waveManagement.maxAmmo - 90))
                {
                    playerShoot.ammo += (playerShoot.waveManagement.maxAmmo - playerShoot.ammo);
                    Destroy(this.gameObject);
                }

            }
        }
    }
}
