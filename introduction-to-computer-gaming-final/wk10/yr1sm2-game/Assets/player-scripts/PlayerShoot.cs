﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour {

    public WaveManagement waveManagement;
    public GameObject bullet;
    ManualMovement manualMovement;
    PlayerMovement playerMovement; // get attackDamage
    public bool objectExists, fromLeft, fromRight, fire;
    float fireTime, timer;
    public int ammo;

	// Use this for initialization
	void Start () {
        manualMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<ManualMovement>();
        waveManagement = GameObject.Find("Game").GetComponent<WaveManagement>();
        fireTime = 0.1f; // fire rate for gun
        fromLeft = true;
        fromRight = false;
        fire = false;
        ammo = waveManagement.maxAmmo;
    }
	
	// Update is called once per frame
	void Update () {
		if (fire)
        {
            timer += Time.deltaTime;
            // print(fire);
            FireBullet();
            fire = false;
        }
	}
    public void FireBullet()
    {
        // if statements for alternating fire
        // if ammunition is not zero
        if (fromLeft && !fromRight && timer >= fireTime && ammo > 0)
        {
            timer = 0f;
            GameObject newBullet = Object.Instantiate(GetBullet(), (this.gameObject.transform.position + (this.gameObject.transform.forward *2f) + (this.gameObject.transform.up * 0.3f) - (this.gameObject.transform.right * 0.22f)), transform.rotation) as GameObject;
            newBullet.GetComponentInChildren<Rigidbody>().velocity = newBullet.transform.forward * 99;
            fromRight = true;
            fromLeft = false;
            ammo--;
            
        }
        if (fromRight && !fromLeft && timer >= fireTime)
        {
            timer = 0f;
            GameObject newBullet = Object.Instantiate(GetBullet(), (this.gameObject.transform.position + (this.gameObject.transform.forward *2f)+ (this.gameObject.transform.up * 0.3f) + (this.gameObject.transform.right * 0.3f)), transform.rotation) as GameObject;

            newBullet.GetComponentInChildren<Rigidbody>().velocity = newBullet.transform.forward * 99;
            fromLeft = true;
            fromRight = false;
            ammo--;
            
        }
    }
    public GameObject GetBullet()
    {
        return bullet;
    }
}
