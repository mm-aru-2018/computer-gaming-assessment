﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResumeOnClick : MonoBehaviour {

    public GamePause gamePause;

    private void Awake()
    {
        gamePause = GameObject.Find("Game").GetComponent<GamePause>();
        if (gamePause == null) // "second pass" to try and find relevant component
        {
            gamePause = GameObject.Find("Game").GetComponent<GamePause>();
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Resume()
    {
        gamePause.ResumeGame();
    }

}
