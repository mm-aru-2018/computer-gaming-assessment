﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIExitPrompt : MonoBehaviour {

    /*
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    */
    public void LogExit()
    {
        Debug.Log("Application terminated by player.");
        Application.Quit();
    }


}
