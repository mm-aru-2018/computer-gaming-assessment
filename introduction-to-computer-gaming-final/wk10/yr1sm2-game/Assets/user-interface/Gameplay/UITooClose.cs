﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITooClose : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (this.isActiveAndEnabled)
        {
            Invoke("DisableThis", 5f);
        }
	}
    void DisableThis()
    {
        this.gameObject.SetActive(false);
    }
}
