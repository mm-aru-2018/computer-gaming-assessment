﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpObject : MonoBehaviour {
    Vector3 A, B;
    float lerpDuration;
    // Use this for initialization
    void Start () {
        A = new Vector3(-2,1.5f,-2);
        B = new Vector3(3, 2, 3);
	}
	
	// Update is called once per frame
	void Update () {
        lerpDuration = Time.time;
        print(lerpDuration);
        // move object back and forth once every approximately 2.5 seconds
        // when use 0.2f, object would move back and forth once every 5 seconds
        this.transform.position = Vector3.Lerp(A,B,Mathf.PingPong(0.4f * lerpDuration, 1)); // https://answers.unity.com/questions/1346360/how-to-move-an-object-back-and-forth-along-a-vecto.html (16/02/2018)
        // https://answers.unity.com/questions/14288/can-someone-explain-how-using-timedeltatime-as-t-i.html
        // last value corresponds to which vector object is to be at, with 0 = A and 1 = B
    }
}
