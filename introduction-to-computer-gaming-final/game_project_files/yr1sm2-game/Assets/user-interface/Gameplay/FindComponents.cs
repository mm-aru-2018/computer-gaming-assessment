﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FindComponents : MonoBehaviour {
    // Find components for UI

    public WaveManagement waveManagement;
    public GameObject player, pSpawn, canvas;
    public GameObject[] interfaceElements;

    // find UI elements for countdown
    public GameObject[] countdown;

    // find UI elements to warn player that they can't move
    public GameObject[] tooClose;
    public GameObject[] tooLong;


    public PlayerHealth playerHealth;
    public PlayerShoot playerShoot;
    public PlayerSpawn playerSpawn;
    public int phealth;
    public bool healthFound;
    public GameObject gameplayUI, gameOverUI, gameCompleteUI;
    public float timer;
    public bool UIActivated = false;

    // Use this for initialization
    void Awake()
    {
        /*
        tooClose[0].SetActive(false);
        tooClose[1].SetActive(false);
        tooLong[0].SetActive(false);
        tooLong[1].SetActive(false);
        */
        canvas = GameObject.Find("Canvas");
        gameplayUI = GameObject.Find("GameplayUI");
        gameOverUI = GameObject.Find("GameOverUI");
        gameCompleteUI = GameObject.Find("GameComplete");
        countdown = GameObject.FindGameObjectsWithTag("Countdown");
        tooClose = GameObject.FindGameObjectsWithTag("UITooClose");
        tooLong = GameObject.FindGameObjectsWithTag("DelayShoot");
        gameOverUI.SetActive(false);
        gameplayUI.SetActive(false);
        gameCompleteUI.SetActive(false);
        waveManagement = GetComponent<WaveManagement>();
        
    }



    void Start () {
        
        pSpawn = GameObject.Find("PSpawnVolume");

        // find wave management component
        
        if (pSpawn == null)
        {
            pSpawn = GameObject.Find("PSpawnVolume");
            
            
            // Debug.LogError("Player not found");
            //player = GameObject.FindGameObjectWithTag("Player");
        } else if (pSpawn != null)
        {
            Debug.Log("Spawn found!");
            playerSpawn = pSpawn.GetComponent<PlayerSpawn>();
        }


        if (playerSpawn.playerExists)
        {
            
            if (playerHealth == null)
            {
                Debug.LogError("Can't find playerhealth");
            }
                if (player == null)
            {
                Debug.LogError("Can't find player");
                // phealth = playerHealth.currentHealth;
            } else if (player != null)
            {
                player = GameObject.FindGameObjectWithTag("Player");
                
                playerHealth = player.GetComponent<PlayerHealth>();
                playerShoot = player.GetComponentInChildren<PlayerShoot>();
                Debug.Log("Player found!");
            }
            
        } else
        {
            Debug.Log("Player still doesn't exist");
        }



    }
	
	// Update is called once per frame
	void Update () {
        
        if (playerSpawn.playerExists)
        {
            // Debug.Log("Player now exists");
            healthFound = FindPlayerHealth();
            ActivateCanvas();
            // DisableCanvas();
        }
	}
    bool FindPlayerHealth()
    {
        //player = GameObject.FindGameObjectWithTag("Player");
        //playerHealth = player.GetComponent<PlayerHealth>();
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
            playerHealth = player.GetComponent<PlayerHealth>();
            playerShoot = player.GetComponentInChildren<PlayerShoot>();
            
            if (playerHealth == null)
            {
                playerHealth = player.GetComponent<PlayerHealth>();
            }
            return false;
        } else if (playerHealth != null && player != null)
        {
            return true;
        }
        return false;
    }
    void ActivateCanvas()
    {
        if (playerHealth != null)
        {
            if (!UIActivated) // do not activate game ui continuously for pause menu to work
            {
                gameplayUI.SetActive(true);
                UIActivated = true;
            }
            timer += Time.deltaTime;
            if (timer <= 2f)
            {
                // stop unnecessary UI elements from showing up
                tooClose[0].SetActive(false);
                tooClose[1].SetActive(false);
                tooLong[0].SetActive(false);
                tooLong[1].SetActive(false);
            } else if (timer > 2f)
            {
                // stop timer from incrementing.
                timer += 0f;
            }
            // gameplayUI = GameObject.Find("GameplayUI");
        }
        
    }
    
    void DisableCanvas()
    {
        bool isgActive = true;
        if (!playerSpawn.playerExists)
            {
                gameplayUI.SetActive(false);
            
            isgActive = false;
            if (!isgActive)
            {
                gameplayUI.SetActive(false);
            }
            } else if (playerHealth != null){
                gameplayUI.SetActive(true);
            
        }
    }
}
