﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleMovement : MonoBehaviour {
    public Toggle toggle;
    public PlayerPrefs playerMovePrefs;
    public int auto; // hold value of playerprefs key in memory
	// Use this for initialization
	void Start () {
        toggle = this.GetComponent<Toggle>();

        // retain setting across sessions?
        // check to see if automove has been toggled or set from first game launch
        if (PlayerPrefs.HasKey("automove"))
        {
            auto = PlayerPrefs.GetInt("automove");
            if (auto == 1) // if enabled last time
            {
                toggle.isOn = true; // keep enabled
                auto = 2;
                // PlayerPrefs.DeleteKey("automove");

            }
            else if (auto == 0) // if disabled from last time
            {
                toggle.isOn = false; // keep disabled
                auto = 2;
                // PlayerPrefs.DeleteKey("automove");

            }
        }
    }
	
	// Update is called once per frame
	void Update () {
        UseToggle();
    }

    public void UseToggle()
    {
        // PlayerPrefs.DeleteKey("automove");


        if (toggle.isOn)
        {
            PlayerPrefs.SetInt("automove", 1);
            PlayerPrefs.Save();
            Debug.Log("Auto movement enabled");
        }
        else if (!toggle.isOn)
        {
            PlayerPrefs.SetInt("automove", 0);
            PlayerPrefs.Save();
            Debug.Log("Auto movement disabled");
        }
    }

}
