﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ContinueFromMenu : MonoBehaviour {

    [SerializeField]
    string _sceneName;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ContinueButton()
    {
        SceneManager.LoadScene(_sceneName);
        Time.timeScale = 1.0f; // in case new game is started from pause menu
    }
}
