﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EnableContinue : MonoBehaviour {
    public GameObject _playerWave;
    public PlayerWave playerWave;
    public Button button;

    public GameObject pW, pWprefab;

    private void Awake()
    {
        // get PlayerWave prefab
        pW = GameObject.FindGameObjectWithTag("Waves");

        if (pW == null)
        {
            pW = Object.Instantiate(GetPrefab(), Vector3.zero, new Quaternion(0, 0, 0, 0)) as GameObject;
        }
        if (pW != null)
        {
            playerWave = pW.GetComponent<PlayerWave>();

        }
    }

    // Use this for initialization
    void Start () {
        _playerWave = GameObject.FindGameObjectWithTag("Waves");
        if (_playerWave != null)
        {
            playerWave = _playerWave.GetComponent<PlayerWave>();
        }
        button = GameObject.Find("ContinueButton").GetComponent<Button>();
        
        
    }
	
	// Update is called once per frame
	void Update () {
        ContinueButton();
	}


    GameObject GetPrefab()
    {
        return pWprefab;
    }

    public void ContinueButton()
    {
        // disable Continue button if playerWave does not exist or is less than or equal to one
        if (playerWave == null || playerWave.waveNo <= 1)
        {
            button.interactable = false;
        }
        if (playerWave != null)
        {
            if (playerWave.waveNo > 1)
            {
                button.interactable = true;
            }
        }


        // SceneManager.LoadScene(_sceneName);
        // Time.timeScale = 1.0f; // in case game is started from pause menu
    }

}
