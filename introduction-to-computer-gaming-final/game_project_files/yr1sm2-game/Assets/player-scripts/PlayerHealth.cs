﻿using System.Collections;
using System.Collections.Generic;
// using UnityEngine.UI; // allow player health to be shown in user interface
using UnityEngine;

public class PlayerHealth : MonoBehaviour {

    public AudioSource hitSource;

    public int startingHealth = 100;
    public int currentHealth;
    EnemyAttack enemyAttack;
    GameObject enemy;
    public float timer;
    WaveManagement waveManagement;

    PlayerMovement playerMovement; // reference to player movement - stop the player from moving when dead.

    // PlayerShooting playerShooting;

    public bool isDead = false;
    public bool damaged;

    void Awake()
    {
        waveManagement = GameObject.Find("Game").GetComponent<WaveManagement>();
        // set up references
        enemy = GameObject.FindGameObjectWithTag("Enemy");
        playerMovement = GetComponent<PlayerMovement>(); // have reference point to component
        
        currentHealth = startingHealth;
        if (enemy != null)
        {
            enemyAttack = enemy.GetComponent<EnemyAttack>();
        }
    }

	// Use this for initialization
	void Start () {
        hitSource = transform.GetChild(0).GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

        DrainHealth();
        ResetKillstreak();
        
	}
    private void OnDestroy()
    {
        //Cursor.visible = true;
        // Cursor.lockState = CursorLockMode.Confined
    }
    public void ResetKillstreak()
    {
        if (damaged && currentHealth <= 75) // if shot and health is equal to or less than 75
        {
            // reset killstreak if below certain health and hit
            waveManagement.killstreak = 0; // set killstreak back to zero
        }
    }
    public void TakeDamage (int amount) // enemy player calls this function; argument reflects how much damage the player has taken.
    {      
        if (currentHealth <= 0 && !isDead)
        {
            Death();
        } else if (currentHealth > 0) // make player take damage if hit
        {
            currentHealth -= amount;
            return;
        } else
        {
            isDead = false;
        }
    }
    public void DrainHealth()
    {
        // if current health is lower than 50
        if (currentHealth < 50)
        {
            
            if (timer >= 5f)
            {
                timer = 0f;
                currentHealth--;
            }
        } else
        {
            return;
        }
    }
    void Death()
    {
        isDead = true;
        //playerShooting.DisableEffects() // disable player shooting effects
        playerMovement.enabled = false; // don't let the player move when dead.
        // playerShooting.enabled = false;
        Debug.Log("Player is dead");
        
    }
    private void OnTriggerEnter(Collider other)
    {
        // if enemy bullet hits player's object trigger volume
        if (other.gameObject.tag == "EnemyBullet")
        {
            if (!hitSource.isPlaying) {
                hitSource.PlayOneShot(hitSource.clip, 0.1f); // play sound to let player know they're hit
            }
            damaged = true; // invoke damaged

            
            Debug.Log("Player hit" + " " + damaged);
        } else if (other.gameObject.tag == null)
        {
            if (hitSource.isPlaying)
            {
                hitSource.Stop();
            }
            damaged = false;
        }
        if (other.gameObject.name == "ToolBoxPrefab")
        {
            Debug.Log("interacting with toolbox");
        }
    }
}
