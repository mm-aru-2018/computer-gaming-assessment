﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SpawnGame : MonoBehaviour {

    GameObject capCube, player;
    GameObject newcapCube;
    TestScript[] testScript = new TestScript[0];
    Vector3 randVec;
    float randX, randZ;
    int[] a = new int [10];

    PlayerHealth playerHealth;

	// Use this for initialization
	void Start () {

        // find each instance of this script per game object, supposedly
        testScript = gameObject.GetComponents<TestScript>();
        Array.Resize<TestScript>(ref testScript, 10); // resize array
        Debug.Log(testScript.Length);

        player = GameObject.FindGameObjectWithTag("Player");
        capCube = GameObject.Find("CapcubePrefab");

        playerHealth = player.GetComponent<PlayerHealth>();

        for (int i = 0; i < 10; i++)
        {
            // spawn objects randomly
            randX = UnityEngine.Random.Range(-50f, 50f);
            randZ = UnityEngine.Random.Range(-50f, 50f);
            randVec = new Vector3(randX, 1f, randZ);
            newcapCube = Instantiate(capCube, randVec, Quaternion.identity);

            // taken from here: https://answers.unity.com/questions/575575/getcomponent-on-multiple-objects-in-an-array.html
            
            // for each index in array, get this script (in child game object)
            testScript[i] = capCube.GetComponentInChildren<TestScript>();

        }
        for (int i = 0; i < testScript.Length; i++)
        {
            // get location of spawned enemies
        //    Debug.Log(testScript[i].location);
            a[i] = testScript[i].amount;
        //    Debug.Log(a[i]);
            
        }
        Destroy(capCube); // destroy original prefab
	}
	
	// Update is called once per frame
	void Update () {

	}

    void IfDead()
    {
        if (playerHealth.currentHealth == 0)
        {
            player.transform.Translate(Vector3.down * 2);
            GameObject newObject = Instantiate(player) as GameObject;
            Debug.Log("created");

        }
    }

}
