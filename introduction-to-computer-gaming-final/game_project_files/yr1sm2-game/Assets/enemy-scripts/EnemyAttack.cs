﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour {

    public AudioClip shootSound;
    public AudioSource source;

    public Animator anim; // find animator
    WaveManagement waveManagement;
    float timeBetweenAttacks = 0.1f; // enemy's "fire rate"
    public int bulletsFired;
    //public float timeBetweenAttacks = 0.5f; // "enemy fire time"
    float reloadTime = 2.5f;
    public int attackDamage; // public variables override console variable values.
    // Animator anim;
    public GameObject bullet;
    public GameObject player, enemySpawnV, defense;
    public PlayerHealth playerHealth; // reference to Player game object
    EnemyHealth enemyHealth;
    EnemyMovement enemyMovement;
    EnemySpawn enemySpawn;
    public DefensesHealth defensesHealth;
    public PBulletDestroy pBulletDestroy;
    public bool playerInRange;
    float timer;
    public float maxDistShoot = 20f;
    Collider bulletCollider;
    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    // Use this for initialization
    void Start () {
        source = GetComponent<AudioSource>();
        waveManagement = GameObject.Find("Game").GetComponent<WaveManagement>();
        attackDamage = (int)waveManagement.attackdmg;
        // set up reference
        // also try to find player if confirm want to try again
        player = GameObject.FindGameObjectWithTag("Player"); // do once to improve game performance
        if (player != null)
        {
            playerHealth = player.GetComponent<PlayerHealth>(); // refer to player script in order to be able to take away player health
        }
        enemyHealth = GetComponent<EnemyHealth>(); // refer to this component so as to be able to stop attacking the player when they are dead
        enemyMovement = GetComponent<EnemyMovement>(); // refer to this component - so as to be able to calculate distance from player to shoot
        enemySpawnV = GameObject.Find("ESpawnVolume");
        bulletsFired = 0;
    }
	
	// Update is called once per frame
	void Update () {
        
        // timer for attack intervals
        timer += Time.deltaTime; // accumulate timer in seconds to determine how much time passed.
        // print(timer);

        // cause harm to player with intervals and if close enough
        if (timer >= timeBetweenAttacks && enemyMovement.DistanceFromPlayer() <= maxDistShoot && enemyHealth.currentHealth > 0 && bulletsFired < 3) // long enough between attacks
        {
            anim.SetTrigger("stopshoot");
            // spawn bullet
            Attack();
            
        } else if (bulletsFired == 3 && timer >= reloadTime/* || enemyMovement.DistanceFromPlayer() > maxDistShoot*/) // simulate enemy reloading
        {
            anim.SetTrigger("shoot");
            anim.SetTrigger("stopshoot");
            bulletsFired = 0;
        }
        if (playerHealth.currentHealth <= 0 && playerHealth == null)
        {
            anim.SetTrigger("shoot");
            anim.SetTrigger("stopshoot");
            Destroy(this.gameObject, 10f); // destroy enemy
            // player is dead
        }
        
    }
    
    void Attack() // harm player
    {
        // spawn bullets
        timer = 0f; // allow for repeated fire by resetting the timer
        
        bulletsFired++;
        if (playerHealth.currentHealth > 0)
        {
            GameObject bulletObject = GetBullet();
            pBulletDestroy = bulletObject.GetComponentInChildren<PBulletDestroy>(); // get health of defenses via collision
            defensesHealth = pBulletDestroy.defensesHealth;

            // spawn bullets https://answers.unity.com/questions/746960/instantiate-object-in-front-of-player.html
            GameObject newBullet = Object.Instantiate(bulletObject, (this.gameObject.transform.position + this.gameObject.transform.forward + (this.gameObject.transform.up * 1.5f)), transform.rotation) as GameObject;
            // newBullet.transform.position += this.gameObject.transform.forward * Time.deltaTime;
            // move bullet forward
            newBullet.GetComponentInChildren<Rigidbody>().velocity = newBullet.transform.forward * 33;
            source.PlayOneShot(shootSound, 0.05f);
            // pBulletHarm = newBullet.GetComponentInChildren<PBulletHarm>();

            

            // defensesHealth = bulletCollider.gameObject.GetComponent.defensesHealth;
            
            //https://unity3d.com/learn/tutorials/temas/multiplayer-networking/shooting-single-player
            //https://forum.unity.com/threads/box-collider-not-detecting-collisions.196932/
            if (playerHealth != null)
            {
                if (playerHealth.damaged)
                {
                    // attackDamage = (int)waveManagement.attackdmg;
                    playerHealth.TakeDamage(attackDamage);
                    playerHealth.damaged = false; // stop player from taking damage, especially when not being shot through defenses
                }
            }
            if (defensesHealth != null)
            {
                if (defensesHealth.damaged)
                {
                    defensesHealth.TakeDamage(attackDamage);
                    print(defensesHealth.currentHealth);
                }
            } else if (defensesHealth == null)
            {
                pBulletDestroy = bulletObject.GetComponentInChildren<PBulletDestroy>(); // get health of defenses via collision

                // defensesHealth = bulletCollider.gameObject.GetComponent.defensesHealth;
                defensesHealth = pBulletDestroy.defensesHealth;
            }
            Destroy(newBullet, 2f);
        }
        

    }

    // calculate distance
    public float Distance(Vector3 a, Vector3 b)
    {
        // take the delta values from the vector points
        float deltaX = b.x - a.x;
        float deltaY = b.y - a.y;
        float deltaZ = b.z - a.z;

        // square the delta values
        deltaX = Mathf.Pow(deltaX, 2);
        deltaY = Mathf.Pow(deltaY, 2);
        deltaZ = Mathf.Pow(deltaZ, 2);

        // add up and return delta values for each axes
        float dist = deltaX + deltaY + deltaZ;
        return dist;
    }

    GameObject GetBullet()
    {
        return bullet;
    }

}
