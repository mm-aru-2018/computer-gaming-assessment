﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    public AudioClip dyingSound;
    public AudioSource source;
    public AudioSource hitSource;

    public GameObject healthPickUp, ammoPickUp;
    public EnemyMovement enemyMovement;
    public Animator anim; // find animator
    WaveManagement waveManagement;
    public int startingHealth = 100;
    public int currentHealth;
    // public float sinkSpeed = 2.5f; // speed at which enemy sinks through floor when dead
    public int scoreValue = 10;

    PlayerHealth playerHealth;

    // ParticleSystem hitParticles; // reference to the particle system that plays when enemy damaged
    CapsuleCollider capsuleCollider;
    public bool isDead, finalHit;
    // bool isSinking;

        

    void Awake()
    {
        // set up references
        // hitParticles = GetComponentInChildren <ParticleSystem>();
        capsuleCollider = GetComponent<CapsuleCollider>();
        waveManagement = GameObject.Find("Game").GetComponent<WaveManagement>();
        currentHealth = startingHealth;
        anim = GetComponent<Animator>();
        isDead = false;
        finalHit = false;
    }

    GameObject GetHealth()
    {
        return healthPickUp;
    }
    GameObject GetAmmo()
    {
        return ammoPickUp;
    }

	// Use this for initialization
	void Start () {
        hitSource = transform.GetChild(1).GetComponent<AudioSource>();
        source = GetComponent<AudioSource>();
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        enemyMovement = GetComponent<EnemyMovement>(); // refer to this component - so as to be able to calculate distance from player to shoot
    }
	
	// Update is called once per frame
	void Update () {
        DestroyEnemy();
	}

    void DestroyEnemy()
    {
        if (isDead)
        {
            // do not count any more enemies killed
            if (!finalHit)
            {
                // play dying sound
                source.PlayOneShot(dyingSound, 0.1f);

                // play dying animation
                anim.SetTrigger("deadEn");
                
                // spawn health and ammo
                Object.Instantiate(GetHealth(), this.gameObject.transform.position + Vector3.up, this.gameObject.transform.rotation);
                Object.Instantiate(GetAmmo(), this.gameObject.transform.position + Vector3.up + (Vector3.forward), this.gameObject.transform.rotation);

                // count as enemy killed
                waveManagement.enemiesRemaining--;

                // increase killstreak
                waveManagement.killstreak++;

                // stop enemy from taking any more damage to prevent issues with animations and navmesh agent from accidentally being activated
                finalHit = true;
            }
            
            Destroy(this.gameObject, 5f);
            
        } else if (playerHealth.currentHealth == 0)
        {
            // destroy enemy for interface purposes, but do not count it as enemies killed
            // Debug.Log("Reached wave " + waveManagement.waveNo + ", " + waveManagement.enemiesRemaining + " enemies remained");
            Destroy(this.gameObject, 0f);
            
        }
    }

    // kill specific enemy
    public void TakeDamage(int amount/*, Vector3 hitPoint*/) // call from another function
    {

        // enemyAudio.Play(); // play hurt sound effect
        
        if (currentHealth <= 0)
        {
            capsuleCollider.enabled = false; // disable collider to avoid animation bugs
            // anim.SetBool("isEnemyDead", true);
            
            isDead = true;
            
            return; // do not take any more damage
            
        }
        if (currentHealth > 0)
        {
            
            anim.SetTrigger("hit"); // glitchy


            currentHealth -= amount;
        }
        print(currentHealth);
        // hitParticles.transform.position = hitPoint;

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PlayerBullet")
        {
            if (!hitSource.isPlaying)
            {
                hitSource.PlayOneShot(hitSource.clip, 0.8f);
            }
            Debug.Log("Enemy hit");
        }
    }

}
