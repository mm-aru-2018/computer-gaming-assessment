﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UINewGame : MonoBehaviour {

    [SerializeField]
    string _sceneName;
    public GameObject _playerWave;
    public PlayerWave playerWave;

	// Use this for initialization
	void Start () {
        _playerWave = GameObject.FindGameObjectWithTag("Waves");
        if (_playerWave != null)
        {
            playerWave = _playerWave.GetComponent<PlayerWave>();
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlayNewGame()
    {
        
        SceneManager.LoadScene(_sceneName);
        if (_sceneName == "GameScene" && playerWave != null)
        {
            // reset wave when New Game clicked
            playerWave.newGameStarted = true;
            playerWave.Save();
        }
        Time.timeScale = 1.0f; // in case new game is started from pause menu
    }

}
