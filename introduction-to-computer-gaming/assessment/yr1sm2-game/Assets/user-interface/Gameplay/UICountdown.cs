﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UICountdown : MonoBehaviour {
    public FindComponents findComponents;
    public Text textUI;
    public EnemySpawn enemySpawn;

	// Use this for initialization
	void Start () {
        textUI = this.GetComponent<Text>();
        findComponents = GameObject.Find("Game").GetComponent<FindComponents>();
        
    }
	
	// Update is called once per frame
	void Update () {
        if (this.isActiveAndEnabled == true)
        {
            textUI.text = ("Prepare to fight, in:\n" + ((int)findComponents.waveManagement.enemySpawn.countdownTime));
        }
	}
}
