﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// save works everywhere except web
using System.Runtime.Serialization.Formatters.Binary; // write binary file that can't be modified by player
using System.IO; // file management for C#

public class PlayerWave : MonoBehaviour {
    
    public WaveManagement waveManagement;
    public EnableContinue enableContinue;
    public GameObject duplicate;
    public Scene currentScene;
    public int waveNo;
    public bool newGameStarted = false;
    
    // check if player has clicked continue = pass a variable?
    // disable script if new game clicked?

    private void Awake()
    {
        
        DontDestroyOnLoad(gameObject);
        // waveManagement = GameObject.Find("Game").GetComponent<WaveManagement>();
        Load();
        Debug.Log("Loading from file");
        
        /*if (newGameStarted )
        {

        }
        else if (waveNo != 0)
        {
            waveNo += 0;
        }*/

        // define waveNo!
        if (waveNo == 0)
        {
            waveNo = 1;
        }

    }

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        currentScene = SceneManager.GetActiveScene();

        // search for game object after changing settings in menu
        if (waveManagement == null && currentScene.name == "GameScene")
        {
            waveNo += 0;
            waveManagement = GameObject.Find("Game").GetComponent<WaveManagement>();
        }

        // if waveManagement component found
        if (waveManagement != null)
        {
            waveNo = waveManagement.waveNo;
        }
        // if player clicks on New Game
        if (newGameStarted || waveNo == 0)
        {
            waveNo = 1;
            newGameStarted = false;
        }
    }

    // save and overwrite game data
    public void Save()
    {
        // create binary formatter
        BinaryFormatter bf = new BinaryFormatter();
        Debug.Log(Application.persistentDataPath); // print location of save file

        // create file at Unity3D persistent data path named playerInfo.at
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat"); // persistent data path, hidden like e.g. AppData with Windows

        // 
        PlayerData data = new PlayerData
        {
            waveNo = waveNo
        };

        // write to file and serialize data
        bf.Serialize(file, data); // write serializable data to file
        file.Close();
    }

    // load game data
    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat")){

            // for deserialization
            BinaryFormatter bf = new BinaryFormatter();
            Debug.Log(Application.persistentDataPath); // print location of save file
            
            // open file from location
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open); // persistent data path, hidden like e.g. AppData with Windows

            // set data as deserialized contents of opened file
            PlayerData data = (PlayerData) bf.Deserialize(file); // cast from object to PlayerData class
            file.Close();

            // set the current wave number as deserialized value of class above
            waveNo = data.waveNo;
        }
    }

}

// create a class that holds the current wave number
[System.Serializable] // make serializable
class PlayerData
{
    public int waveNo;
}
