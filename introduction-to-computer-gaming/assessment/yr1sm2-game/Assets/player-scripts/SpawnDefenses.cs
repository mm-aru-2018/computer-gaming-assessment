﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDefenses : MonoBehaviour {
    public GameObject block;
    public int blocksSpawned, timesRespawned;
    bool startSpawn;
    GameObject rotator, newBlock;
    public List<GameObject> defensesSpawned = new List<GameObject>();
    // Use this for initialization
    void Start () {
        blocksSpawned = 0;
        startSpawn = true;
        timesRespawned = 0; // respawn a different block each time
        // https://docs.unity3d.com/ScriptReference/Transform.GetChild.html
        // rotator = this.gameObject.transform.GetChild(0).GetChild(0).GetChild(0).gameObject;
	}
	
	// Update is called once per frame
	void Update () {
        startSpawn = true;
		if (Input.GetButtonDown("Fire3"))
        {
            print(defensesSpawned.Count);
            print(defensesSpawned);
            if (blocksSpawned < 4 && defensesSpawned.Count < 4)
            {
                // GameObject newBlock = Object.Instantiate(GetBlock()) as GameObject;
                // defensesSpawned.Add(GameObject newBlock = Object.Instantiate(GetBlock()) as GameObject);
                // Destroy(newBlock);
                // newBlock.transform.position = this.gameObject.transform.forward;
                
                newBlock = Object.Instantiate(GetBlock(), this.gameObject.transform.position + this.gameObject.transform.forward * 3, transform.rotation) as GameObject;
                defensesSpawned.Add(newBlock);
                

                // https://answers.unity.com/questions/746960/instantiate-object-in-front-of-player.html
                // newBlock.transform.position = this.gameObject.transform.position + this.gameObject.transform.forward;
                blocksSpawned++;
                
                
                startSpawn = false;
                
            } else if (blocksSpawned >= 4 && defensesSpawned.Count >= 4) // shuffle blocks
            {
                // https://docs.unity3d.com/ScriptReference/Transform.Find.html
                // new Quaternion(0,gameObject.transform.rotation.y,0,0)

                // destroy last block in list, eventually moving to first
                Destroy(defensesSpawned[defensesSpawned.Count - 1 - timesRespawned]);
                defensesSpawned.RemoveAt(defensesSpawned.Count - 1 - timesRespawned);
                blocksSpawned--;

                // spawn new block and append to list
                // https://answers.unity.com/questions/746960/instantiate-object-in-front-of-player.html
                newBlock = Object.Instantiate(GetBlock(), this.gameObject.transform.position + this.gameObject.transform.forward * 3, transform.rotation) as GameObject;
                defensesSpawned.Add(newBlock);
                                
                // newBlock.transform.position = this.gameObject.transform.position + this.gameObject.transform.forward;
                blocksSpawned++;
                timesRespawned++;
                startSpawn = false;

                if (timesRespawned >= 4)
                {
                    timesRespawned = 0;
                }

            }
            // if (blocksSpawned >= 4 && defensesSpawned.Count >= 4 && )
            
        }
        
	}
    GameObject GetBlock()
    {
        
        return block;
    }
}
