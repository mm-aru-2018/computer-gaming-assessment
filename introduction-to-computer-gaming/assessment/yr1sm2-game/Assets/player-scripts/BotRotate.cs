﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotRotate : MonoBehaviour {

    Vector3 position, newPosition;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Rotate();
		
	}

    void Rotate()
    {
        //Game Object is moving
        if (Vector3.Distance(transform.position, position) < 1)
        {
            //  transform.Rotate(0, -90f, 0);
            Quaternion newRotation = Quaternion.LookRotation(position - transform.position, Vector3.right);


            newRotation.x = 0f;
            newRotation.z = 0f;
            newRotation.y = -90f;

            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * 10);
            // controller.SimpleMove(transform.right * speed);
            transform.position = Vector3.Lerp(transform.position, newPosition, 0.5f);

        }
        //Game Object is not moving
        else
        {

        }
    }

}
