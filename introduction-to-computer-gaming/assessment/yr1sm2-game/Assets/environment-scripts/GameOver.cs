﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour {

    

    WaveManagement waveManagement;
    FindComponents findComponents;
    PlayerSpawn playerSpawn;
    public bool playerSpawned = false; // prevent game over state from occurring at start of game
    public bool isGameOver = false; // prevent player from being able to use pause menu when game over
    public bool cursorUnlocked;
    // Use this for initialization
    private void Awake()
    {
        cursorUnlocked = false;
        waveManagement = GameObject.Find("Game").GetComponent<WaveManagement>();
        playerSpawn = GameObject.Find("PSpawnVolume").GetComponent<PlayerSpawn>();
        findComponents = GetComponent<FindComponents>();
    }
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        // if player dies before killing all enemies in final wave
		if (!playerSpawn.playerExists && playerSpawned && waveManagement.waveNo <= 5 && waveManagement.enemiesRemaining > 0)            
        {
            // trigger to disable pause menu
            isGameOver = true;
            Debug.Log("Reached wave " + waveManagement.waveNo + ", " + waveManagement.enemiesRemaining + " enemies remained");
            Debug.Log("Game Over");

            // enable cursor
            Cursor.visible = true;
            // Cursor.lockState = CursorLockMode.Confined;
            // https://answers.unity.com/questions/123647/how-to-detect-mouse-movement-as-an-input.html
            if (cursorUnlocked || Input.anyKey || Input.GetAxis("Mouse X") > 0 || Input.GetAxis("Mouse Y") > 0)
            {
                
                Cursor.lockState = CursorLockMode.None;
                Cursor.lockState = CursorLockMode.Confined;
            }
            // hide and show relevant UI elements
            findComponents.gameplayUI.SetActive(false);
            findComponents.gameOverUI.SetActive(true);
        } else if (waveManagement.enemiesRemaining <= 0 && waveManagement.waveNo >= 5)
            // else if game is complete
        {
            // trigger to disable pause menu
            isGameOver = true;
            // enable cursor
            Cursor.visible = true;
            playerSpawn.playerMovement.enabled = false;
            playerSpawn.camMouseLook.enabled = false;
            // Cursor.lockState = CursorLockMode.Confined;
            if (cursorUnlocked || Input.anyKey || Input.GetAxis("Mouse X") > 0 || Input.GetAxis("Mouse Y") > 0)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.lockState = CursorLockMode.Confined;
            }
            // hide and show relevant UI elements
            findComponents.gameplayUI.SetActive(false);
            findComponents.gameCompleteUI.SetActive(true);
            
            // game complete!
        }
	}
}
