﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WaveManagement : MonoBehaviour {
    public int waveNo, enemiesRemaining, killstreak, noEnemies, maxAmmo, playerDmg;
    public EnemySpawn enemySpawn;
    public PlayerSpawn playerSpawn;
    public GameObject UICountdown;
    public GameObject[] playerWaves;
    public GameObject pW, pWprefab;


    public PlayerWave playerWave; // handle waves between scenes
    

    public float attackdmg, ksTimer;
    public bool refillAmmoBonus, waveIncremented, damageIncremented;
    // Use this for initialization
    private void Awake()
    {


        // get PlayerWave prefab
        pW = GameObject.FindGameObjectWithTag("Waves");
        
        if (pW == null)
        { // if playerWave doesn't exist, instantiate
            pW = Object.Instantiate(GetPrefab(), Vector3.zero, new Quaternion(0, 0, 0, 0)) as GameObject;
        } // if playerWave exists
        if (pW != null)
        {
            playerWave = pW.GetComponent<PlayerWave>();
        }

        // wave number - SAVE FILE OVERWRITES THIS

        // set wave number to 1 or PlayerWave value
        if (waveNo == 0)
        {
            waveNo = playerWave.waveNo;
        }

        // get player spawn in order to get player damage attributes
        playerSpawn = GameObject.Find("PSpawnVolume").GetComponent<PlayerSpawn>();

        // get enemy spawn in order to get enemy attributes
        enemySpawn = GameObject.Find("ESpawnVolume").GetComponent<EnemySpawn>();

        // get this object to display countdown
        UICountdown = GameObject.Find("Countdown");
    }
    void Start () {
        waveIncremented = false; // to allow saving to a game file
        damageIncremented = false;
        refillAmmoBonus = false;
        // find enemy spawn volume


        
        killstreak = 0;

        // number of enemies to spawn each wave

        // if statement for when player is continuing game
        if (waveNo < 4) {
            noEnemies = 10; // starting number of enemies
        } else if (waveNo == 4 || waveNo == 5)
        {
            noEnemies = 15; // starting number of enemies
        }
        
        enemiesRemaining = noEnemies;

        // set enemy attack damage - increment per wave.
        attackdmg = 0f;

        // set maximum ammo
        maxAmmo = 500;
        // set player damage - save file maybe overwrites this with larger value?
        playerDmg = 20;

        // read file data and load larger values 

        // SAVE WAVE DATA WHENEVER PLAYER ACHIEVES IT
        // SEPARATE CODE WHEN PLAYER STARTS NEW GAME

        /* IF WAVENO > 1 OR PLAYERDMG > 20 AND THEN IF WAVENO OR DAMAGE INCREMENTED, 
        SAVE TO/OVERWRITE FILE AND ENABLE CONTINUE BUTTON IN MAIN MENU (IF NOT DONE ALREADY)

        IF CONTINUE SELECTED FROM MAIN MENU, REFER TO SAVE FILE AND OVERWRITE WAVEMANAGEMENT.CS VALUES

        ELSE IF NEW GAME SELECTED FROM MAIN MENU, DELETE SAVE FILE?
        */

	}

    GameObject GetPrefab()
    {
        return pWprefab;
    }

    // Update is called once per frame
    void Update () {

        if (waveIncremented)
        {
            playerWave.Save();
            Debug.Log("Saving...");
        }

        switch (waveNo)
        {
            case 1:
                waveIncremented = false;
                noEnemies = 10;
                attackdmg = 1f;
                //enemySpawn._ok = true;
                if (enemiesRemaining == 0 && !waveIncremented) //advance to next wave and respawn enemies, save progress if not done so
                {
                    waveIncremented = true;
                    waveNo++;
                    enemySpawn._ok = true;
                    enemiesRemaining = noEnemies; // "reset" enemies remaining counter for the player's benefit
                }
                break;
            case 2:
                // waveIncremented = false;
                noEnemies = 10;
                attackdmg = 2f;
                if (enemiesRemaining == 0 && !waveIncremented)
                {
                    waveIncremented = true;
                    waveNo++;
                    enemySpawn._ok = true;
                    enemiesRemaining = noEnemies;
                }
                break;
            case 3:
                // waveIncremented = false;
                noEnemies = 10;
                attackdmg = 3f;
                if (enemiesRemaining == 0 && !waveIncremented)
                {
                    waveIncremented = true;
                    waveNo++;
                    enemySpawn._ok = true;
                    enemiesRemaining = 15;
                }
                break;
            case 4:
                // waveIncremented = false;
                noEnemies = 15;
                attackdmg = 2f;
                if (enemiesRemaining == 0 && !waveIncremented)
                {
                    waveIncremented = true;
                    waveNo++;
                    enemySpawn._ok = true;
                    enemiesRemaining = 15;
                }
                break;
            case 5:
                // waveIncremented = false;
                noEnemies = 15;
/*                if (noEnemies != enemiesRemaining)
                {
                    enemiesRemaining = noEnemies;
                }*/
                // enemiesRemaining = 15;
                attackdmg = 3f;
                
                if (enemiesRemaining == 0 && !waveIncremented)
                {
                    break;
                }
                break;
            default:
                noEnemies = 0;
                attackdmg = 0f;
                break;
        }
        UpgradePlayer();

        
	}
    void UpgradePlayer()
    {
        damageIncremented = false;
        if (killstreak >= 7 && playerDmg <100 && !damageIncremented)
        {
            damageIncremented = true;
            // upgrade player's damage
            playerSpawn.playerMovement.damageModified = false;
            playerDmg += 20;
            print(playerDmg);
            killstreak = 0; 
        }
        
    }
}
