﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour
{
    GameObject player;
    Camera mcamera, pcamera;
    AudioListener macamera, pacamera; // to manually disable audio listener
    PlayerHealth playerHealth;

    void Awake()
    {
        mcamera = Camera.main;
        macamera = mcamera.GetComponent<AudioListener>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
        pcamera = player.GetComponentInChildren<Camera>();
        pacamera = pcamera.GetComponent<AudioListener>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CheckPlayerAlive();
    }
    public void CheckPlayerAlive()
    {
        if (player != null)
        {
            if (playerHealth.currentHealth > 0)
            {
                SwitchPlayer();
            }
        }
        else if (playerHealth.isDead || player == null)
        {
            SwitchMain();
        }
        
    }
    public void SwitchMain()
    {
        mcamera.enabled = true;
        macamera.enabled = true;
        pcamera.enabled = false;
        pacamera.enabled = false;
    }
    public void SwitchPlayer()
    {
        mcamera.enabled = false;
        macamera.enabled = false;
        pcamera.enabled = true;
        pacamera.enabled = true;
    }
}
