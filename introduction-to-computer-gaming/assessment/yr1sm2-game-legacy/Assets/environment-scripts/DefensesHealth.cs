﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefensesHealth : MonoBehaviour {
    public int startingHealth = 100;
    public int currentHealth;
    EnemyAttack enemyAttack;
    GameObject enemy;
    public bool damaged;
    private void Awake()
    {
        enemy = GameObject.FindGameObjectWithTag("Enemy");
        currentHealth = startingHealth;
        enemyAttack = enemy.GetComponent<EnemyAttack>();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void TakeDamage(int amount)
    {
        if (currentHealth <= 0)
        {
            Destroy(this.gameObject);
        } else
        {
            currentHealth -= amount;
            return;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "EnemyBullet")
        {
            damaged = true;
            // Debug.Log("Defenses hit");
        } else
        {
            damaged = false;
        }
    }
}
