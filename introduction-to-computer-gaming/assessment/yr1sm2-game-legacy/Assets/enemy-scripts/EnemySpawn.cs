﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {

    float _timeBetweenSpawns = 5f;
    [SerializeField] // required to get prefab
    GameObject _prefab, pSpawnVolume;
    // GameObject[] _prefabs;

    BoxCollider collider;

    // struct - tells space collider takes up - point in space to spawn
    Bounds _spawnBounds;

    float currentTimer = 0f;

    // don't instantiate any more prefabs when a prefab is spawned
    bool _ok;
    PlayerSpawn playerSpawn;
    EnemyAttack enemyAttack;

    // Use this for initialization
    void Start () {
        _ok = true;

        collider = this.gameObject.GetComponent<BoxCollider>();
        if (collider == null)
        {
            Debug.LogError("No Box Collider attached to game object");
            _ok = false;
        }
        else
        {
            // get the box collider's physical boundaries
            _spawnBounds = collider.bounds;
        }

        // check if there are any prefabs requested from the engine
        if (_prefab == null)
        {
            Debug.LogError("No prefabs attached.");
            _ok = false;
        }
        pSpawnVolume = GameObject.Find("PSpawnVolume");
        playerSpawn = pSpawnVolume.GetComponent<PlayerSpawn>();
    }
	
	// Update is called once per frame
	void Update () {
        SpawnEnemies();
    }

    // check if enemies killed - surround below function with this

    void SpawnEnemies()
    {
        if (_ok)
        {
            //currentTimer += Time.deltaTime;


            // code from source uses local values, so add current position to random range
            float spawnX = Random.Range(-_spawnBounds.extents.x + transform.position.x, _spawnBounds.extents.x + transform.position.x);
            // float spawnX = transform.position.x;
            float spawnY = transform.position.y;
            float spawnZ = transform.position.z;

            // spawn object - cast as game object
            for (int i = 0; i < 1; i++) // modify with wave number?
            {
                GameObject newItem = Object.Instantiate(GetPrefab()) as GameObject;
                newItem.transform.position = new Vector3(spawnX, spawnY, spawnZ);
                if (playerSpawn.playerExists) // destroy enemy when player is dead and confirms respawn
                {
                    // Destroy(newItem, 2f);
                    enemyAttack = newItem.GetComponent<EnemyAttack>();
                    enemyAttack.player = GameObject.FindGameObjectWithTag("Player");
                    enemyAttack.playerHealth = enemyAttack.GetComponent<PlayerHealth>();
                }
            }
            _ok = false;
        }
    }

    private GameObject GetPrefab()
    {
        return _prefab;
    }

    // destroy existing enemies and "reset counter" if player wants to try again?
}
