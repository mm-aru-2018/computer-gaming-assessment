﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PBulletDestroy : MonoBehaviour {
    public DefensesHealth defensesHealth;
    GameObject enemy;
    EnemyAttack enemyAttack;
    // Use this for initialization
    void Start () {
        // reluctant to invoke this - will only refer to first spawned enemy's attack value
        enemy = GameObject.FindGameObjectWithTag("Enemy");
        enemyAttack = enemy.GetComponent<EnemyAttack>();
	}
	
	// Update is called once per frame
	void Update () {
        
	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {

            // Debug.Log("hit");
            // https://answers.unity.com/questions/275343/destroy-parent-of-child-gameobject.html
            Destroy(this.transform.parent.gameObject);
        } else
        {
            Destroy(this.transform.parent.gameObject, 1f);
        }
        
        if (other.gameObject.tag == "Defences")
        {
            defensesHealth = other.GetComponent<DefensesHealth>();
            if (defensesHealth != null)
            {
            //    Debug.Log("Defenses hit");
                // reluctant to do this
                defensesHealth.TakeDamage(enemyAttack.attackDamage);
            } else if (defensesHealth == null)
            {
                Debug.Log("Defenses missing");
            }
            // Destroy(this.transform.parent.gameObject);
        }
    }
}
