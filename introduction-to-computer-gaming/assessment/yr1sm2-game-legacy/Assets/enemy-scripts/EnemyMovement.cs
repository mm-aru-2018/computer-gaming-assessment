﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour {
    Transform player; // reference to the player's position
    PlayerHealth playerHealth; // reference to the player's health
    EnemyHealth enemyHealth; // reference to the enemy's health
    NavMeshAgent nav; // reference to the navmesh
    Collider spherecollider;
    EnemyAttack enemyAttack;

    float timer;

    Vector3 randomDirection; // for random movement when player is out of range of enemy
    public float wanderRadius;
    public float wanderTimer;
    public float maxDistFollow = 30f; // maximum distance that the enemy will follow the player

    float randX, randZ;

    float n;

    void Awake()
    {
   

    }

    // Use this for initialization
    void Start () {

        // set up references
        player = GameObject.FindGameObjectWithTag("Player").transform;
        playerHealth = player.GetComponent<PlayerHealth>();
        enemyHealth = GetComponent<EnemyHealth>();
        nav = GetComponent<NavMeshAgent>();
        //spherecollider = GetComponent<SphereCollider>();
        enemyAttack = GetComponent<EnemyAttack>();

        n = nav.stoppingDistance;
	}
	
	// Update is called once per frame
	void Update () {
        
        timer += Time.deltaTime;
        // print(timer);
        // print(DistanceFromPlayer());
        // if the enemy and the player have health left
        if (enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
        {
        // move enemy to player using navmesh
            nav.enabled = true;

            // if calculated distance is equal to that of the maximum distance defined for following the player
            
            if (DistanceFromPlayer() <= maxDistFollow)
            {
                nav.isStopped = false;
                nav.SetDestination(player.position);
            }

            // if the player is too far away...
            if (DistanceFromPlayer() > maxDistFollow) // do not place with an else condition or program will only choose betwefen moving and not moving throughout
            {

            // nav.isStopped = true;
            SetRandomPosition();
                // move enemy to random position

                // nav.isStopped = true;
            }

        } else if (enemyHealth.currentHealth == 0 || playerHealth.currentHealth == 0) // if either current enemy or player is dead, inclusive
        {
            // stop enemy from moving
            nav.isStopped = true;
            // disable navmesh
            // nav.enabled = false;
        } else if (DistanceFromPlayer() <= n && playerHealth.currentHealth > 0) // try to move away from player
        {
            nav.SetDestination(new Vector3(player.position.x * RandomPlusMinus() * n, player.position.y, player.position.z * RandomPlusMinus() * n));
        }
	}
    public float DistanceFromPlayer()
    {
        if (player != null)
        {
            Vector3 enemyCoords = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, this.gameObject.transform.position.z); // https://docs.unity3d.com/ScriptReference/Transform-position.html
            float fx = player.position.x - enemyCoords.x;
            float fy = player.position.y - enemyCoords.y;
            float fz = player.position.z - enemyCoords.z;
            return Mathf.Sqrt((Mathf.Pow(fx, 2)) + (Mathf.Pow(fy, 2)) + (Mathf.Pow(fz, 2)));
        } else
        {
            return 0f;
        }
    }

    public void SetRandomPosition()
    {
        
        if (timer >= 5f) // move enemy every five seconds - does so independently to "seek" player
        {
            timer = 0; // reset timer
            Vector3 newVect = Random.insideUnitSphere * 50; // https://docs.unity3d.com/ScriptReference/Random-insideUnitSphere.html
            nav.SetDestination(newVect);
        }
    }
    
    int RandomPlusMinus()
    {
        float i = Random.Range(0f, 1f);
        if (i < 0.5f)
        {
            return -1;
        } else
        {
            return 1;
        }
    }
}
