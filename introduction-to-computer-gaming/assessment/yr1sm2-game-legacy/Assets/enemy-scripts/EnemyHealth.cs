﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {
    public int startingHealth = 100;
    public int currentHealth;
    // public float sinkSpeed = 2.5f; // speed at which enemy sinks through floor when dead
    public int scoreValue = 10;

    // ParticleSystem hitParticles; // reference to the particle system that plays when enemy damaged
    CapsuleCollider capsuleCollider;
    public bool isDead;
    // bool isSinking;

    void Awake()
    {
        // set up references
        // hitParticles = GetComponentInChildren <ParticleSystem>();
        capsuleCollider = GetComponent<CapsuleCollider>();

        currentHealth = startingHealth;
        isDead = false;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        DestroyEnemy();
	}

    void DestroyEnemy()
    {
        if (currentHealth == 0)
        {
            Destroy(this.gameObject, 5f);
        }
    }


    // kill specific enemy
    public void TakeDamage(int amount/*, Vector3 hitPoint*/) // call from another function
    {

        // enemyAudio.Play(); // play hurt sound effect

        
        if (currentHealth == 0)
        {
            isDead = true;
            
            return; // do not take any more damage
            
        } else
        {
            currentHealth -= amount;
        }
        print(currentHealth);
        // hitParticles.transform.position = hitPoint;

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PlayerBullet")
        {
            Debug.Log("Enemy hit");
        }
    }

}
