﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour {

    public GameObject bullet;
    PlayerMovement playerMovement; // get attackDamage
    public bool objectExists, fromLeft, fromRight, fire;
    float fireTime, timer;

	// Use this for initialization
	void Start () {
        fireTime = 0.075f; // fire rate for gun
        fromLeft = true;
        fromRight = false;
        fire = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButton("Fire1") || fire)
        {
            timer += Time.deltaTime;
            // print(fire);
            FireBullet();
        }
	}
    public void FireBullet()
    {
        // if statements for alternating fire
        // if ammunition is not zero
        if (fromLeft && !fromRight /*&& timer >= fireTime*/)
        {
            timer = 0f;
            GameObject newBullet = Object.Instantiate(GetBullet(), (this.gameObject.transform.position + (this.gameObject.transform.forward *2f) + (this.gameObject.transform.up * 0.3f) - (this.gameObject.transform.right * 0.22f)), transform.rotation) as GameObject;
            newBullet.GetComponentInChildren<Rigidbody>().velocity = newBullet.transform.forward * 99;
            fromRight = true;
            fromLeft = false;
            
        }
        if (fromRight && !fromLeft /*&& timer >= fireTime*/)
        {
            timer = 0f;
            GameObject newBullet = Object.Instantiate(GetBullet(), (this.gameObject.transform.position + (this.gameObject.transform.forward *2f)+ (this.gameObject.transform.up * 0.3f) + (this.gameObject.transform.right * 0.3f)), transform.rotation) as GameObject;

            newBullet.GetComponentInChildren<Rigidbody>().velocity = newBullet.transform.forward * 99;
            fromLeft = true;
            fromRight = false;
            
        }
    }
    public GameObject GetBullet()
    {
        return bullet;
    }
}
