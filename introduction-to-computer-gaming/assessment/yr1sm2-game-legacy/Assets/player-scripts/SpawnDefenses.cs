﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDefenses : MonoBehaviour {
    public GameObject block;
    int blocksSpawned;
    bool startSpawn;
    GameObject rotator;
	// Use this for initialization
	void Start () {
        blocksSpawned = 0;
        startSpawn = true;
        // https://docs.unity3d.com/ScriptReference/Transform.GetChild.html
        // rotator = this.gameObject.transform.GetChild(0).GetChild(0).GetChild(0).gameObject;
	}
	
	// Update is called once per frame
	void Update () {
        startSpawn = true;
		if (Input.GetButtonDown("Fire3"))
        {

            if (blocksSpawned == 0)
            {
                GameObject newBlock = Object.Instantiate(GetBlock()) as GameObject;
                // Destroy(newBlock);
                newBlock.transform.position = this.gameObject.transform.forward;
                blocksSpawned++;
                startSpawn = false;
                
            } else
            {
                // https://docs.unity3d.com/ScriptReference/Transform.Find.html
                // new Quaternion(0,gameObject.transform.rotation.y,0,0)
                GameObject newBlock = Object.Instantiate(GetBlock(), this.gameObject.transform.position + this.gameObject.transform.forward * 3, transform.rotation) as GameObject;
                
                // https://answers.unity.com/questions/746960/instantiate-object-in-front-of-player.html
                // newBlock.transform.position = this.gameObject.transform.position + this.gameObject.transform.forward;
                blocksSpawned++;
                startSpawn = false;
            }
            
        }
        
	}
    GameObject GetBlock()
    {
        
        return block;
    }
}
