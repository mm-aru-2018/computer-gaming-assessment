﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour {

    // float _timeBetweenSpawns = 5f;
    [SerializeField] // required to get prefab
    GameObject _prefab;
    // GameObject[] _prefabs;

    BoxCollider collider;

    // struct - tells space collider takes up - point in space to spawn
    Bounds _spawnBounds;

    //float currentTimer = 0f;

    // don't instantiate any more prefabs when a prefab is spawned
    public bool _ok, playerExists; // check if player can spawn


    // camera code
    GameObject player;
    Camera mcamera, pcamera;
    AudioListener macamera, pacamera; // to manually disable audio listener
    PlayerHealth playerHealth;
    EnemyAttack enemyAttack;

    // Use this for initialization
    void Start () {

        enemyAttack = GetComponent<EnemyAttack>();

        mcamera = Camera.main;
        macamera = mcamera.GetComponent<AudioListener>();

        _ok = true;

        collider = GetComponent<BoxCollider>();
        if (collider == null)
        {
            Debug.LogError("No Box Collider attached to game object");
            _ok = false;
        } else
        {
            // get the box collider's physical boundaries
            _spawnBounds = collider.bounds;
        }

        // check if there are any prefabs requested from the engine
        if (_prefab == null)
        {
            Debug.LogError("No prefabs attached.");
            _ok = false;
        }

	}
	
	// Update is called once per frame
	void Update () {

        // if able to spawn a prefab
        CheckPlayerExists();
	}

    private GameObject GetPrefab()
    {
        return _prefab;
    }

    // check if player wants to try again - surround the below function with this.

    void CheckPlayerExists()
    {

        if (_ok || (player == null && Input.GetKeyDown("y")))
            {
                //currentTimer += Time.deltaTime;


                float spawnX = Random.Range(-_spawnBounds.extents.x + transform.position.x, _spawnBounds.extents.x + transform.position.x);
                float spawnY = transform.position.y;
                float spawnZ = transform.position.z;

                // spawn object - cast as game object
                GameObject newItem = Object.Instantiate(GetPrefab()) as GameObject;
                newItem.transform.position = new Vector3(spawnX, spawnY, spawnZ);


                player = GameObject.FindGameObjectWithTag("Player");
                playerHealth = player.GetComponent<PlayerHealth>();
                pcamera = player.GetComponentInChildren<Camera>();
                pacamera = pcamera.GetComponent<AudioListener>();

                playerExists = true;
                _ok = false;
            }
        if (player != null) // stop function from happening to prevent calls to player camera when player is dead - function is within Update()
        {
            if (playerHealth.currentHealth > 0 && player != null)
            {
                SwitchPlayer();
            }
            else if (playerHealth.currentHealth <= 0)
            {
                playerExists = false;
                SwitchMain();
            }
        }

    }

    public void SwitchMain()
    {
        pcamera.enabled = false;
        pacamera.enabled = false;
        mcamera.enabled = true;
        macamera.enabled = true;
        
        if (!pacamera.enabled && !pcamera.enabled)
        {
            Destroy(player, 0.5f);
        }
        
    }
    public void SwitchPlayer()
    {
        mcamera.enabled = false;
        macamera.enabled = false;
        pcamera.enabled = true;
        pacamera.enabled = true;
    }

}
