﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {
    public WaveManagement waveManagement;
    public FindComponents findComponents;
    float _timeBetweenSpawns = 20f;
    public float countdownTime;
    [SerializeField] // required to get prefab
    GameObject _prefab, pSpawnVolume;

    // GameObject[] _prefabs;

    BoxCollider collider;

    // struct - tells space collider takes up - point in space to spawn
    Bounds _spawnBounds;

    float currentTimer = 0f;

    // don't instantiate any more prefabs when a prefab is spawned
    public bool _ok, showCountdown;
    PlayerSpawn playerSpawn;
    EnemyAttack enemyAttack;

    // Use this for initialization
    void Start () {
        findComponents = GameObject.Find("Game").GetComponent<FindComponents>();
        waveManagement = GameObject.Find("Game").GetComponent<WaveManagement>();
        
        _ok = true;

        collider = this.gameObject.GetComponent<BoxCollider>();
        if (collider == null)
        {
            Debug.LogError("No Box Collider attached to game object");
            _ok = false;
        }
        else
        {
            // get the box collider's physical boundaries
            _spawnBounds = collider.bounds;
        }

        // check if there are any prefabs requested from the engine
        if (_prefab == null)
        {
            Debug.LogError("No prefabs attached.");
            _ok = false;
        }
        pSpawnVolume = GameObject.Find("PSpawnVolume");
        playerSpawn = pSpawnVolume.GetComponent<PlayerSpawn>();
    }
	
	// Update is called once per frame
	void Update () {
        if (_ok)
        {
            // set countdown time to 20
            countdownTime = _timeBetweenSpawns;

            // call function with delay
            Invoke("SpawnEnemies", _timeBetweenSpawns);

            // decrement it "according to" above function
            
            
            // SpawnEnemies();
            _ok = false;
        }
        if (countdownTime > 0)
        {
            findComponents.countdown[0].SetActive(true);
            findComponents.countdown[1].SetActive(true);
            countdownTime -= Time.deltaTime;
            // Debug.Log(countdownTime);
        } else if (countdownTime <= 0)
        {
            findComponents.countdown[0].SetActive(false);
            findComponents.countdown[1].SetActive(false);
            countdownTime -= 0;
        }
    }

    // check if enemies killed - surround below function with this

    void SpawnEnemies()
    {
        //currentTimer += Time.deltaTime;


        // code from source uses local values, so add current position to random range
        float spawnX = Random.Range(-_spawnBounds.extents.x + transform.position.x, _spawnBounds.extents.x + transform.position.x);
        // float spawnX = transform.position.x;
        float spawnY = transform.position.y;
        float spawnZ = transform.position.z;

        // spawn object - cast as game object

        // spawn enemies
        for (int i = 0; i < waveManagement.noEnemies; i++) // modify with wave number?
        {
            GameObject newItem = Object.Instantiate(GetPrefab()) as GameObject;
            newItem.transform.position = new Vector3(spawnX, spawnY, spawnZ);
            if (playerSpawn.playerExists) // destroy enemy when player is dead and confirms respawn
            {
                // Destroy(newItem, 2f);
                enemyAttack = newItem.GetComponent<EnemyAttack>();
                enemyAttack.player = GameObject.FindGameObjectWithTag("Player");
                enemyAttack.playerHealth = enemyAttack.GetComponent<PlayerHealth>();
                enemyAttack.attackDamage = (int)waveManagement.attackdmg;
            }
        }
        _ok = false;
        /*if (_ok)
        {

        }*/
    }

    private GameObject GetPrefab()
    {
        return _prefab;
    }

    // destroy existing enemies and "reset counter" if player wants to try again?
}
