﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManualMovement : MonoBehaviour {

    PlayerShoot playerShoot;

	// Use this for initialization
	void Start () {
        playerShoot = this.gameObject.GetComponentInChildren<PlayerShoot>();
	}
	
	// Update is called once per frame
	void Update () {

        // rotate player
        if (Input.GetAxis("Horizontal") > 0)
        {
            this.gameObject.transform.localEulerAngles += new Vector3(0, 90f, 0) * Time.deltaTime;
        }
        if (Input.GetAxis("Horizontal") < 0)
        {
            this.gameObject.transform.localEulerAngles -= new Vector3(0, 90f, 0) * Time.deltaTime;
        }

        // move player in direction that it is facing
        // https://gamedev.stackexchange.com/questions/101025/moving-a-character-depending-on-the-direction-he-is-facing-c-unity3d
        if (Input.GetAxis("Vertical") > 0)
        {
            this.gameObject.transform.position += this.transform.forward * 5f * Time.deltaTime;
        }
        if (Input.GetAxis("Vertical")< 0)
        {
            this.gameObject.transform.position -= this.transform.forward * 5f * Time.deltaTime;
        }
        if (Input.GetButton("Fire1"))
        {
            playerShoot.fire = true;
        }
    }
}
