﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI; // for NavMeshAgent to work

public class PlayerMovement : MonoBehaviour
{

    //[SerializeField]
    public GameObject bullet;

    // enable parts of code that allow automatic movement
    public bool autoMovement;

    // private Animator anim;

    WaveManagement waveManagement;
    FindComponents findComponents;
    NavMeshAgent navMeshAgent;
    private Vector3 position;
    public static Vector3 cursorPosition;
    public CharacterController controller;
    public float speed;
    private Vector3 newPosition;

    // shooting and animation variables - ignore
    public float shootDistance = 10f;
    public float shootRate = .5f;
    // private Transform targetedEnemy; // coordinates shoot enemy
    private Ray shootRay;
    private RaycastHit shootHit; // info about what hit
    // private bool walking; // walk animation playing
    // private bool enemyClicked;
    // private float nextFire;

    GameObject robotBody;
    CamMouseLook camMouseLook;
    GameObject enemy, pl, gun;
    Camera cm;
    EnemyHealth enemyHealth;
    Vector3 lookPos;
    Quaternion rt;
    public PlayerShoot playerShoot;

    float timeBetweenAttacks = 0.05f; // simulate a fast but limited fire rate
    float timer;
    public int attackDamage = 20;
    int shootableMask; // shoot only on shootable layer
    int defaultMask, defensesMask;

    public bool damageModified;

    //Ray gshootRay; // ray from gun end forwards (possibly)
    //RaycastHit gsr;

    // for setup - do before "start"
    void Awake()
    {
        waveManagement = GameObject.Find("Game").GetComponent<WaveManagement>();
        findComponents = GameObject.Find("Game").GetComponent<FindComponents>();
        // set up references
        // get components and apply to field
        navMeshAgent = GetComponent<NavMeshAgent>();
        shootableMask = LayerMask.GetMask("Shootable");
        pl = GameObject.FindGameObjectWithTag("Player");
        cm = pl.GetComponentInChildren<Camera>(); // refer to camera in children - for use with rays
        // gun = GameObject.Find("Gun");
        camMouseLook = pl.GetComponentInChildren<CamMouseLook>();

        robotBody = GameObject.Find("RobotBody");

        // enemyHealth = GetComponent<EnemyHealth>();
        playerShoot = pl.GetComponentInChildren<PlayerShoot>();
    }

    void Start()
    {
        damageModified = true;
        attackDamage = 20;
        navMeshAgent = gameObject.GetComponent<NavMeshAgent>();
        shootableMask = LayerMask.GetMask("Shootable");
        defaultMask = LayerMask.GetMask("Default");
        defensesMask = LayerMask.GetMask("Defenses");
        pl = GameObject.FindGameObjectWithTag("Player");
        cm = pl.GetComponentInChildren<Camera>(); // refer to camera in children - for use with rays
        controller = pl.GetComponent<CharacterController>();
        // playerShoot = pl.GetComponent<PlayerShoot>();
        autoMovement = true;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (autoMovement)
        {
            
            LocateCursor(); // show player where their mouse is pointing at
            this.transform.position += Vector3.up * 0.3f;
            // cast ray from mouse position from camera to scene
            // https://answers.unity.com/questions/885341/fps-using-mouse-to-move-rather-than-keys.html
            if (Input.GetButtonDown("Fire2")) // https://www.youtube.com/watch?v=GANwdCKoimU
            {
                //Locate and move to where the player clicked on the terrain
                LocatePosition();
            }
            // if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
            // Rotate();
        }
        if (!damageModified)
        {
            attackDamage = (int)waveManagement.playerDmg;
            damageModified = true;
        }

    }

    void LocatePosition()
    {
        Ray ray = cm.ScreenPointToRay(Input.mousePosition); // create a raycast from camera to point in screen
        RaycastHit hit; // info from raycast

        if (Physics.Raycast(ray, out hit, 1000))
        {
            if (hit.collider.tag != "Player" && hit.collider.tag != "Enemy")
            {
                navMeshAgent.SetDestination(hit.point); // move to where player clicked as long as it isn't to enemy
                Debug.Log("You will move to: " + hit.point + ", at a distance of " + (Vector3.Distance(this.gameObject.transform.position, hit.point)));

                // if distance is too close, at 1, tell player they will not move

                // show beacon of where moving

                if (((int)Vector3.Distance(this.gameObject.transform.position, hit.point)) == 1){
                    findComponents.tooClose[0].SetActive(true);
                    findComponents.tooClose[1].SetActive(true);
                    Debug.Log("You have requested a position point too near from the current/previously requested position. Please try again.");
                }

                /*
                lookPos = hit.point - transform.position;
                lookPos.y = 0;

                rt = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.Slerp(transform.rotation, rt, speed);
                */
                // Rotate();

            }
        }
    }

    // get current position of mouse cursor - where player looking at
    Vector3 LocateCursor()
    {
        // shoot ray from attached object's position to where aim
        Ray ray = cm.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit; // info from above variable

        // shoot ray from attached object's position to where aim
        Debug.DrawLine(this.transform.position, cursorPosition, Color.red, 1000f);
        if (Physics.Raycast(ray, out hit, 1000))
        {
            cursorPosition = hit.point; // have cursor position be equal to hit point - useful for debug
            
            // shootRay from end of gun (may have to change) and forward from barrel

            // attached to camera since root object lacks any reference to a raycast.
            shootRay.origin = cm.transform.position;
            shootRay.direction = cm.transform.forward;
            
            
            // https://answers.unity.com/questions/384355/stopping-a-raycast-after-hit.html
            // shootableMask removed, as it would shoot through the defense systems
            if (Physics.Raycast(shootRay, out shootHit, hit.point.magnitude))
            {
                // print(shootHit.collider.tag);
                // try to find the enemyhealth script - costly on performance, however
                if (shootHit.collider.tag == "Enemy")
                {
                    // start timer to check if player is automatically shooting enemy
                    timer += Time.deltaTime;

                    // enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();



                    // playerShoot.FireBullet();



                    // if the above component exists


                    print("Looking at enemy");
                    enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();
                    if (enemyHealth != null)
                    {
                        
                        // print("enemyHealth component found!");
                        // if the enemy's health is greater than zero and if the timer is greater than time between attacks
                        if (enemyHealth.currentHealth > 0/* && timer > timeBetweenAttacks*/)
                        {
                            
                            print("Looking at enemy");

                            // debug purposes
                            if (timer >= 3f)
                            {
                                findComponents.tooLong[0].SetActive(true);
                                findComponents.tooLong[1].SetActive(true);
                                Debug.Log("Taking too long to kill enemy... Try moving elsewhere to shoot.");
                                // timer = 0f;
                            }

                            playerShoot.fire = true;
                        } else if (enemyHealth.currentHealth <= 0)
                        {
                            playerShoot.fire = false;
                        }

                    }
                    else if (enemyHealth == null)
                    {
                        
                        playerShoot.fire = false;
                        Debug.Log("Enemy not found");
                    }

                    
                }
                if (shootHit.collider.tag != "Enemy")
                {
                    // reset and stop timer to check if player is automatically shooting enemy
                    timer = 0f;
                    timer += 0f;
                    //Debug.Log("Not shooting enemy");
                    playerShoot.fire = false;
                }
            }
                
            
        }
        return cursorPosition; // return values
    }

    // rotate

}
