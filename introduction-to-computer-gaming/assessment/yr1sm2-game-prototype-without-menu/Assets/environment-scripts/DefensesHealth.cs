﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefensesHealth : MonoBehaviour {
    public int startingHealth = 100;
    public int currentHealth;
    EnemyAttack enemyAttack;
    GameObject enemy, player;
    SpawnDefenses spawnDefenses;
    public bool damaged;

    public int index = 0;
    
    private void Awake()
    {
        enemy = GameObject.FindGameObjectWithTag("Enemy");
        player = GameObject.FindGameObjectWithTag("Player");
        if (enemy != null)
        {
            enemyAttack = enemy.GetComponent<EnemyAttack>();
        }
        currentHealth = startingHealth;
        if (player != null)
        {
            spawnDefenses = GameObject.FindGameObjectWithTag("Player").GetComponent<SpawnDefenses>();
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void TakeDamage(int amount)
    {
        if (currentHealth <= 0)
        {
            Destroy(this.gameObject);
            //spawnDefenses.defensesSpawned.RemoveAt(spawnDefenses.blocksSpawned);
            //spawnDefenses.blocksSpawned--;

        } else
        {
            
            currentHealth -= amount;
            return;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "EnemyBullet")
        {
            damaged = true;
            // Debug.Log("Defenses hit");
        } else
        {
            damaged = false;
        }
        if (collision.gameObject.tag == "Defences")
        {
            // tell player that they spawned barricades too close to each other
            // Destroy(collision.gameObject);
        }
    }
}
