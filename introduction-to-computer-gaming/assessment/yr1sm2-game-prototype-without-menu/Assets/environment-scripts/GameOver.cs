﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour {
    WaveManagement waveManagement;
    FindComponents findComponents;
    PlayerSpawn playerSpawn;
    public bool playerSpawned = false; // prevent game over state from occurring at start of game
    // Use this for initialization
    private void Awake()
    {
        waveManagement = GameObject.Find("Game").GetComponent<WaveManagement>();
        playerSpawn = GameObject.Find("PSpawnVolume").GetComponent<PlayerSpawn>();
        findComponents = GetComponent<FindComponents>();
    }
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!playerSpawn.playerExists && playerSpawned)                     
        {
            Debug.Log("Reached wave " + waveManagement.waveNo + ", " + waveManagement.enemiesRemaining + " enemies remained");
            Debug.Log("Game Over");
            Cursor.lockState = CursorLockMode.None;
            findComponents.gameplayUI.SetActive(false);
            findComponents.gameOverUI.SetActive(true);
        }
	}
}
