﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleMovement : MonoBehaviour {
    public float speed = 1f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey("w"))
        {
            // move positively along Z axis
            this.transform.Translate(0, 0, 1 * speed * Time.deltaTime);
        }
        if (Input.GetKey("s"))
        {
            // move negatively along Z axis
            this.transform.Translate(0, 0, -1 * speed * Time.deltaTime);
        }
        if (Input.GetKey("a"))
        {
            // move negatively along X axis
            this.transform.Translate(-1 * speed * Time.deltaTime, 0, 0);
        }
        if (Input.GetKey("d"))
        {
            // move positively along X axis
            this.transform.Translate(1 * speed * Time.deltaTime, 0, 0);
        }
    }
}
