﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMouseLook : MonoBehaviour
{

    Vector2 mouseLook;
    Vector2 smoothV;
    public float sensitivity = 5.0f;
    public float smoothing = 2.0f;

    // move camera when cursor beyond boundaries
    // public float boundary = 50f;
    //private float screenwidth;
    //private float screenheight;

    GameObject character, player, gcamera;
    Vector3 campos;

    float playerX;
    float playerY;
    float playerZ;

    public float crotation;

    // Use this for initialization
    void Start()
    {
        //screenwidth = Screen.width;
        //screenheight = Screen.height;
        // refers to game object
        player = GameObject.Find("Rotator");
        gcamera = this.transform.gameObject;
        character = player.transform.gameObject;
        enabled = true;

        // follow player
        //playerX = player.transform.position.x;
        //playerY = player.transform.position.y + 0.8f;
        //playerZ = player.transform.position.z;

        // campos = player.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        
        gcamera.transform.Rotate(0, -90f, 0);
        /*
        playerX = player.transform.position.x;
        playerY = player.transform.position.y + 0.8f;
        playerZ = player.transform.position.z;
        */
        // camera.transform.position = new Vector3(playerX, playerY, playerZ);

        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y")); // get mouse position

        md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing)); // scale mouse position

        // lerp camera rotation according to mouse movement

        smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / smoothing); // move mouse smoothly by x
        smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / smoothing); // move mouse smoothly by y
        mouseLook += smoothV;
        mouseLook.y = Mathf.Clamp(mouseLook.y, -20.0f, 20f); // prevents from looking upside down

        // rotate around horizontal axis
        transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right); // allows for looking up and down - do not preface with "character" to prevent tilting
        character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up); // set character rotation
        // camera.transform.localRotation = Quaternion.LookRotation(smoothV);

        Cursor.lockState = CursorLockMode.Locked; // hide cursor and keep it within window/screen
            crotation = mouseLook.x;
    }
}
// this is a comment
/*
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));
        smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / smoothing);
        smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / smoothing);
        mouseLook += smoothV;
        mouseLook.y = Mathf.Clamp(mouseLook.y, -90f, 90f); // prevents from looking upside down

        transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);
*/
