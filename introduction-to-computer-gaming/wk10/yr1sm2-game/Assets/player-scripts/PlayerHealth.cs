﻿using System.Collections;
using System.Collections.Generic;
// using UnityEngine.UI; // allow player health to be shown in user interface
using UnityEngine;

public class PlayerHealth : MonoBehaviour {
    public int startingHealth = 100;
    public int currentHealth;
    EnemyAttack enemyAttack;
    GameObject enemy;
    public float timer;
    WaveManagement waveManagement;

    PlayerMovement playerMovement; // reference to player movement - stop the player from moving when dead.

    // PlayerShooting playerShooting;

    public bool isDead = false;
    public bool damaged;

    void Awake()
    {
        waveManagement = GameObject.Find("Game").GetComponent<WaveManagement>();
        // set up references
        enemy = GameObject.FindGameObjectWithTag("Enemy");
        playerMovement = GetComponent<PlayerMovement>(); // have reference point to component
        currentHealth = startingHealth;
        if (enemy != null)
        {
            enemyAttack = enemy.GetComponent<EnemyAttack>();
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

        DrainHealth();
        ResetKillstreak();
        
	}
    public void ResetKillstreak()
    {
        if (damaged && currentHealth <= 75)
        {
            // reset killstreak if below certain health and hit
            waveManagement.killstreak = 0;
        }
    }
    public void TakeDamage (int amount) // enemy player calls this function; argument reflects how much damage the player has taken.
    {      
        if (currentHealth <= 0 && !isDead)
        {
            Death();
        } else if (currentHealth > 0) // make player take damage if hit
        {
            currentHealth -= amount;
            return;
        } else
        {
            isDead = false;
        }
    }
    public void DrainHealth()
    {
        if (currentHealth < 50)
        {
            
            if (timer >= 3f)
            {
                timer = 0f;
                currentHealth--;
            }
        } else
        {
            return;
        }
    }
    void Death()
    {
        isDead = true;
        //playerShooting.DisableEffects() // disable player shootin effects
        playerMovement.enabled = false; // don't let the player move when dead.
        // playerShooting.enabled = false;
        Debug.Log("Do you want to try again? (y/n)");
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "EnemyBullet")
        {
            damaged = true;
            
            Debug.Log("Player hit" + " " + damaged);
        } else if (other.gameObject.tag == null)
        {
            damaged = false;
        }
        if (other.gameObject.name == "ToolBoxPrefab")
        {
            Debug.Log("interacting with toolbox");
        }
    }
}
