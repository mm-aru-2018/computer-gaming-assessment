﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EBulletDestroy : MonoBehaviour {
    EnemyHealth enemyHealth;
    PlayerMovement playerMovement;
    // Use this for initialization
    void Start () {
        playerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
	}
	
	// Update is called once per frame
	void Update () {
        Destroy(this.transform.parent.gameObject, 1f);
	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {

            Debug.Log("hit");
            enemyHealth = other.gameObject.GetComponent<EnemyHealth>();
            
            
            enemyHealth.TakeDamage(playerMovement.attackDamage);
            Destroy(this.transform.parent.gameObject);
            
        } if (other.gameObject.tag == "Defences")
        {
            print("Defenses hit");
            Destroy(this.transform.parent.gameObject);
        }

    }
}
