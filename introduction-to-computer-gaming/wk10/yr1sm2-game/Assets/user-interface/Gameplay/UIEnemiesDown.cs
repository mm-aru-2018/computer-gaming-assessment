﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEnemiesDown : MonoBehaviour {

    public FindComponents findComponents;
    Text textUI;
    // Use this for initialization
    void Start () {
        textUI = GetComponent<Text>();
        findComponents = GameObject.Find("Game").GetComponent<FindComponents>();
    }
	
	// Update is called once per frame
	void Update () {
        textUI.text = ("Enemies remaining: " + findComponents.waveManagement.enemiesRemaining.ToString());
    }
}
