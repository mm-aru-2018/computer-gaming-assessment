﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAmmo : MonoBehaviour {

    public PlayerShoot playerShoot;
    public GameObject pSpawn;
    public PlayerSpawn playerSpawn;
    Text textUI;
    FindComponents findComponents;
    GameObject game;
    // Use this for initialization
    void Start () {
        game = GameObject.Find("Game");
        textUI = this.GetComponent<Text>(); // get Text component
        findComponents = game.GetComponent<FindComponents>(); // find components
        pSpawn = GameObject.Find("PSpawnVolume"); // find player spawn

        playerSpawn = pSpawn.GetComponent<PlayerSpawn>();
        // when player spawned, relay info to UI and FindComponents.
    }
	
	// Update is called once per frame
	void Update () {

        // print current ammo
        textUI.text = (findComponents.playerShoot.ammo.ToString() + "/" + findComponents.waveManagement.maxAmmo.ToString());
    }
}
