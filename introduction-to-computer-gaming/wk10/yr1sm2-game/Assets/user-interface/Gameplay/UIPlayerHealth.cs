﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayerHealth : MonoBehaviour {
    public GameObject player, pSpawn;
    public PlayerSpawn playerSpawn;
    
    
    public PlayerHealth playerHealth;
    Text textUI;
    FindComponents findComponents;
    GameObject game;


    // Use this for initialization
    void Start () {
        game = GameObject.Find("Game");
        textUI = this.GetComponent<Text>(); // get Text component
        findComponents = game.GetComponent<FindComponents>();

        
        pSpawn = GameObject.Find("PSpawnVolume");

        playerSpawn = pSpawn.GetComponent<PlayerSpawn>();


        
        


        if (game == null)
        {
            Debug.LogError("Game scripts not found");
        } else if (game != null)
        {
            if (findComponents == null)
            {
                Debug.LogError("Can't find components and game objects for UI to work");
            }
            Debug.Log("Game scripts found");
        }


        if (textUI == null)
        {
            Debug.LogWarning("No Text UI component attached to this game object.");
        }
	}
	
	// Update is called once per frame
	void Update () {
        // if (GameEventManager.GamePlaying){}


           //player = GameObject.FindGameObjectWithTag("Player");
           //playerHealth = player.GetComponent<PlayerHealth>();

        // show player health
        textUI.text = (findComponents.playerHealth.currentHealth.ToString()+ "/100");
        

    }
    

}
