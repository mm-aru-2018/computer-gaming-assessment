﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddHealth : MonoBehaviour {
    PlayerHealth playerHealth;
    public GameObject player;
    
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        Destroy(this.gameObject, 30f);
	}

    // ensure rigidbody is attached to object for this to work
    private void OnTriggerEnter(Collider other)
    {
        // check if other object is player
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Player interacting with health kit");
            player = other.gameObject;
            if (player != null)
            {
                playerHealth = other.GetComponent<PlayerHealth>();

                // if the player's health is less than 85
                if (playerHealth.currentHealth < 100 && playerHealth.currentHealth < 85)
                {
                    playerHealth.currentHealth += 15;
                    Destroy(this.gameObject);
                    // add remaining health if greater than 85

                }

                // if the player's health is between 85 and 100
                else if (playerHealth.currentHealth > 85)
                {
                    playerHealth.currentHealth += (100 - playerHealth.currentHealth);
                    Destroy(this.gameObject);
                }
                // Destroy(this.gameObject);
            }
        }
    }

}
