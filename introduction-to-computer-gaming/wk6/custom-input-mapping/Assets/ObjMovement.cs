﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjMovement : MonoBehaviour {

    public float accel;
    float minAngle, maxAngle;
    float angle;

	// Use this for initialization
	void Start () {
        accel = 2f;
        minAngle = 0f;
        maxAngle = 90f;
    }
	
	// Update is called once per frame
	void Update () {

        angle = Mathf.LerpAngle(minAngle, maxAngle, Time.time);

        // https://forum.unity.com/threads/input-keys-how-to-reference-horizontal-positive-negative.20483/
        // GetButton won't respond to negative buttons in Unity3D map
        // Buttons below use default mappings
        if (Input.GetAxis("Vertical") > 0)
        {
            this.gameObject.transform.Translate(Vector3.forward * Time.deltaTime * accel);
        }
        if (Input.GetAxis("Vertical") < 0)
        {
            this.gameObject.transform.Translate(Vector3.back * Time.deltaTime * accel);
        }
        if (Input.GetAxis("Horizontal") > 0)
        {
            this.gameObject.transform.Translate(Vector3.right * Time.deltaTime * accel);
        }
        if (Input.GetAxis("Horizontal") < 0)
        {
            this.gameObject.transform.Translate(Vector3.left * Time.deltaTime * accel);
        }
        if (Input.GetButton("Jump"))
        {
            this.gameObject.transform.Translate(Vector3.up * Time.deltaTime * accel);
        }
        if (Input.GetButton("Fire1")) // left ctrl - simulate crouch
        {
            this.gameObject.transform.Translate(Vector3.down * Time.deltaTime * accel);
        }

        // uses negative and positive maps - "Q" and "E" assigned as negative and positive input
        // simulates the "lean" mechanic found in some first person shooters
        if (Input.GetAxis("Fire3") > 0)
        {
            this.gameObject.transform.localEulerAngles = new Vector3(0, 0, 0.5f * -angle);
        }
        if (Input.GetAxis("Fire3") < 0)
        {
            this.gameObject.transform.localEulerAngles = new Vector3(0, 0, 0.5f * angle);
        }
        if (!Input.anyKey) // if keys are released
        {
            this.gameObject.transform.localEulerAngles = Vector3.zero; // reset rotation
        }
    }
}
