/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AIRDASH = 2448450979U;
        static const AkUniqueID AIRWALK = 3638870884U;
        static const AkUniqueID BLASTDOORSOPEN = 2193668060U;
        static const AkUniqueID BLIPHELLO = 2241285296U;
        static const AkUniqueID BLIPLETSGO = 4200252424U;
        static const AkUniqueID BLIPPANIC = 4222839105U;
        static const AkUniqueID BLIPWOO = 3982922089U;
        static const AkUniqueID CACHEVILLE = 2277718743U;
        static const AkUniqueID CHALLENGECOMPLETECACHEVILLE = 3226299631U;
        static const AkUniqueID CHALLENGECOMPLETEHAL = 502664472U;
        static const AkUniqueID CHALLENGECOMPLETEICITY = 1709705167U;
        static const AkUniqueID CHALLENGECOMPLETEKSAN = 4185040146U;
        static const AkUniqueID CHALLENGECOMPLETELANDN = 3384375220U;
        static const AkUniqueID CHALLENGECOMPLETELOGICIEL = 435384469U;
        static const AkUniqueID CHALLENGECOMPLETEMEMORIA = 2299011729U;
        static const AkUniqueID CHALLENGECOMPLETENDAH = 3244120412U;
        static const AkUniqueID CHALLENGECOMPLETEPIXELEY = 3575703361U;
        static const AkUniqueID CHALLENGECOMPLETESIDNEE = 3808564573U;
        static const AkUniqueID CHARACTERSHOP = 2562522030U;
        static const AkUniqueID COLLECTMIP = 3429797041U;
        static const AkUniqueID CRATESHOCK = 2302457228U;
        static const AkUniqueID CREDITS = 2201105581U;
        static const AkUniqueID DAVEATTACK = 1076909391U;
        static const AkUniqueID DAVECLOSING = 126578380U;
        static const AkUniqueID DAVEESCAPE = 3795206082U;
        static const AkUniqueID DAVEGRABDOOR = 2020336589U;
        static const AkUniqueID DAVEHURT = 4228561800U;
        static const AkUniqueID DAVELAUGH = 1993169322U;
        static const AkUniqueID DAVESLAMDOOR = 2506582676U;
        static const AkUniqueID DEATHCRATE = 3308390390U;
        static const AkUniqueID DOUBLEJUMP = 1308315758U;
        static const AkUniqueID EXPLODECRATE = 3577851769U;
        static const AkUniqueID GAMEPAUSED = 935463065U;
        static const AkUniqueID GAMEUNPAUSED = 38601912U;
        static const AkUniqueID HAL = 982177712U;
        static const AkUniqueID ICITY = 2860519191U;
        static const AkUniqueID KSAN = 2684181946U;
        static const AkUniqueID LAN_DN = 2876529975U;
        static const AkUniqueID LANDHEAVY = 2478652267U;
        static const AkUniqueID LANDONPLATFORM = 2042175484U;
        static const AkUniqueID LOGICIEL = 3216519949U;
        static const AkUniqueID MEMORIA = 3803984425U;
        static const AkUniqueID MENUBACK = 2634859593U;
        static const AkUniqueID MENUCHARSELECT = 2559757852U;
        static const AkUniqueID MENUFORWARD = 2780352335U;
        static const AkUniqueID MENUSCROLL = 274288443U;
        static const AkUniqueID MENUSELECT = 664555880U;
        static const AkUniqueID MIPCRATE = 939315594U;
        static const AkUniqueID ND_AH = 3844566325U;
        static const AkUniqueID PASCALSPIN = 2255501185U;
        static const AkUniqueID PETEHELLO = 2941953595U;
        static const AkUniqueID PETELETSGO = 1068317673U;
        static const AkUniqueID PETEPANIC = 1551726642U;
        static const AkUniqueID PETEWOO = 653525094U;
        static const AkUniqueID PHASEWALK = 1409651073U;
        static const AkUniqueID PHASEWALKBLINK = 3391361181U;
        static const AkUniqueID PIXELEY = 400994809U;
        static const AkUniqueID PLAYERDEATH = 1656947812U;
        static const AkUniqueID PLAYERFALLLONG = 1083664599U;
        static const AkUniqueID PLAYERFALLSHORT = 3999073705U;
        static const AkUniqueID PLAYERJUMP = 4008126242U;
        static const AkUniqueID PLAYERPULLUP = 580324632U;
        static const AkUniqueID PLAYERSPRING = 833420393U;
        static const AkUniqueID PLUNKHELLO = 1601912001U;
        static const AkUniqueID PLUNKLETSGO = 1339202579U;
        static const AkUniqueID PLUNKPANIC = 1988280404U;
        static const AkUniqueID PLUNKROAR = 4216099527U;
        static const AkUniqueID PLUNKWOO = 1569987692U;
        static const AkUniqueID POLLYHELLO = 3690404721U;
        static const AkUniqueID POLLYLETSGO = 1419174243U;
        static const AkUniqueID POLLYPANIC = 1980555556U;
        static const AkUniqueID POLLYWOO = 2593737788U;
        static const AkUniqueID POWERSHOP = 1297321652U;
        static const AkUniqueID REWRITECRATE = 3393675388U;
        static const AkUniqueID RMRF_ANNOYED = 309182145U;
        static const AkUniqueID RMRF_GETEM = 593501869U;
        static const AkUniqueID RMRF_LAUGHBIG = 2082691158U;
        static const AkUniqueID RMRF_LAUGHSMALL = 3360214041U;
        static const AkUniqueID RMRFATTACK = 1088148470U;
        static const AkUniqueID RMRFINTRO = 183480432U;
        static const AkUniqueID SHOPBACKWARD = 2722061442U;
        static const AkUniqueID SHOPBUY = 1153606107U;
        static const AkUniqueID SHOPFORWARD = 981230440U;
        static const AkUniqueID SHOPSCROLL = 2491802798U;
        static const AkUniqueID SHOPSCROLLCHARACTER = 36082831U;
        static const AkUniqueID SHOPSCROLLPOWERS = 110212328U;
        static const AkUniqueID SHOPSCROLLSKILLS = 552662120U;
        static const AkUniqueID SHOPSELECT = 1809234257U;
        static const AkUniqueID SHOPSTART = 935390971U;
        static const AkUniqueID SID_NEE = 3122093558U;
        static const AkUniqueID SKILLSHOP = 391291640U;
        static const AkUniqueID SLAMDOWN = 4026462142U;
        static const AkUniqueID SLOWTIME = 1865350373U;
        static const AkUniqueID SUPERJUMP = 188599998U;
        static const AkUniqueID SUREFOOTINGSTART = 1552335306U;
        static const AkUniqueID SURVIVALSTART = 2303223809U;
        static const AkUniqueID TELEPORTEND = 3100814575U;
        static const AkUniqueID TELEPORTSTART = 3389040204U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace GAMESTATES
        {
            static const AkUniqueID GROUP = 777429653U;

            namespace STATE
            {
                static const AkUniqueID CACHEVILLE = 2277718743U;
                static const AkUniqueID CREDITS = 2201105581U;
                static const AkUniqueID HAL = 982177712U;
                static const AkUniqueID ICITY = 2860519191U;
                static const AkUniqueID KSAN = 2684181946U;
                static const AkUniqueID LAN_DN = 2876529975U;
                static const AkUniqueID LOGICIEL = 3216519949U;
                static const AkUniqueID MAINMENU = 3604647259U;
                static const AkUniqueID MEMORIA = 3803984425U;
                static const AkUniqueID ND_AH = 3844566325U;
                static const AkUniqueID PIXELEY = 400994809U;
                static const AkUniqueID RMRFATTACK = 1088148470U;
                static const AkUniqueID RMRFINTRO = 183480432U;
                static const AkUniqueID SHOP = 251412225U;
                static const AkUniqueID SID_NEE = 3122093558U;
                static const AkUniqueID SURVIVALSTART = 2303223809U;
            } // namespace STATE
        } // namespace GAMESTATES

        namespace SHOPSTATES
        {
            static const AkUniqueID GROUP = 2863793047U;

            namespace STATE
            {
                static const AkUniqueID CHARACTERS = 1557941045U;
                static const AkUniqueID IDLE = 1874288895U;
                static const AkUniqueID POWERUPS = 2600048462U;
                static const AkUniqueID SKILLTREE = 387768940U;
            } // namespace STATE
        } // namespace SHOPSTATES

    } // namespace STATES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID MUSICVOLUME = 2346531308U;
        static const AkUniqueID SECTORTUNNEL = 3083512449U;
        static const AkUniqueID SFXVOLUME = 988953028U;
        static const AkUniqueID VOICEVOLUME = 414646191U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MUSIC = 3991942870U;
        static const AkUniqueID SFX = 393239870U;
        static const AkUniqueID SHOP = 251412225U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
        static const AkUniqueID MASTER_SECONDARY_BUS = 805203703U;
    } // namespace BUSSES

}// namespace AK

#endif // __WWISE_IDS_H__
