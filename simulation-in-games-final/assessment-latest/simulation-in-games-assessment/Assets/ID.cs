﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ID : MonoBehaviour {

    public CreateWall createwall;
    int id_;
    Renderer rend;
    Material m;
    // Use this for initialization
    public void Start ()

    {
        // Set transparent in runtime: https://answers.unity.com/questions/1016155/standard-material-shader-ignoring-setfloat-propert.html
        m = new Material(Shader.Find("Standard"));
        m.SetFloat("_Mode", 2);
        m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
        m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        m.SetInt("_ZWrite", 0);
        m.DisableKeyword("_ALPHATEST_ON");
        m.EnableKeyword("_ALPHABLEND_ON");
        m.DisableKeyword("_ALPHAPREMULTIPLY_ON");
        m.renderQueue = 3000;
        id_ = Random.Range(0, 4); // Create a random value (between 0 and 5) for the Id variable 
        // id_Actions();
        createwall = GameObject.Find("CreateWalls").GetComponent<CreateWall>();
        Generate();
        
    }
	
	// Update is called once per frame
	void Update ()
    {
      
    }

    /// <summary>
    /// The use of ID's could be very powerfull and versatile for the creation of large number of the same object with different behaviour. 
    /// For example ,this script will change the color of the material of the object based on its ID.
    /// If you are creating enemies in runtime, the use of ids could determinate the type of enemy you want to create. 
    /// Example: different values of  life, attack and defense of an emey could be link to and ID. 
    ///     ID 0 { life = 4; attack = 3; defense = 6}
    ///     ID 0 { life = 10; attack = 1; defense = 8}
    /// </summary>
    void id_Actions()
    {
        switch(id_)
            {
            case 0:
                
                this.GetComponent<Renderer>().material.color = Color.white;
                break;
            case 1:
                this.GetComponent<Renderer>().material.color = Color.black;
                break;
            case 2:
                this.GetComponent<Renderer>().material.color = Color.red;
                break;
            case 3:
                this.GetComponent<Renderer>().material.color = Color.green;
                break;
            case 4:
                this.GetComponent<Renderer>().material.color = Color.blue;
                break;
            case 5:
                this.GetComponent<Renderer>().material.color = Color.yellow;
                break;
            default:
                break;
        }
    }

    void Generate()
    {
        switch (id_)
        {
            case 0:

                this.GetComponent<Renderer>().material.color = Color.white;
                Destroy(this.gameObject);
                break;
            case 1:
                this.GetComponent<Renderer>().material = m;
                this.GetComponent<Renderer>().material.color = new Color32(255, 0, 0, 133);
                this.gameObject.layer = 8;
                break;
            case 2:
                this.GetComponent<Renderer>().material = m;
                this.GetComponent<Renderer>().material.color = new Color32(255, 0, 0, 133);
                this.gameObject.layer = 12;
                this.gameObject.AddComponent<RandFriction>();
                break;
            case 3:
                this.GetComponent<Renderer>().material = m;
                this.GetComponent<Renderer>().material.color = new Color32(0, 171, 255, 133);
                this.gameObject.layer = 8;
                break;
            default:
                break;
        }
    }
}
