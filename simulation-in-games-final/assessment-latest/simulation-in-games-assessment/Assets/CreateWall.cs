﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreateWall : MonoBehaviour {

    // This script will create a wall of cubes  base on a defined width , height and scale multiplier. 
    public GameObject token;
    public float width;  // Variable to determinate the width of the wall (Dimmension on x axis).  (Used in method CreateWalls2)
    public float height;  // Variable to determiante the height of the wall (Dimmension on y axis) (Used in method CreateWalls2)
    float columns; // Variable to determinate how many cubes will be placed on the X axis.  (Used in method CreateWalls)
    float rows;  // Variable to determinate how many cubes will be placed on the X axis. (Used in method CreateWalls)
    float scale = 0.5f; // Variable to change the scale of each of the individual cubes that builds the wall. (Used in method CreateWalls2)
    Vector3 InitialPosition; // Position to use as reference to place the first cube. 
    Vector3 currentPos; // Vector  to calculate the position of the next cube.
    // Use this for initialization
    void Start ()
    {

        InitialPosition = this.gameObject.transform.position; // Position to use as reference to place the first cube. 
        currentPos = this.gameObject.transform.position; // Vector  to calculate the position of the next cube.
                                           //    width = 20; // How   long is the wall in the x axis. (Used in method CreateWalls2)
                                           //    height = 30; //  How tall the wall is in the Y AXIS. (Used in method CreateWalls2)
        columns = 5;  // How many cubes do you want to put in the x axis of the wall (Used in method CreateWalls)
        rows = 5; // How many cubes do you want to put in the y axis of the wall (Used in method CreateWalls)
                  // CreateWalls();
                  //CreateWalls_Dimmensions();
                  //CreateFloor_Dimmensions();
                  //         RandomFloor();
        GenerateFloor();
    }

    // Update is called once per frame
    void Update () {
        // if (Input.GetKey(KeyCode.Space))  // If user press space, reload the level. 
        //{
        //    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
       // }
    }


    /// <summary>
    ///     CreateWalls
    /// The following method  will create a (rows X columns)  wall made of n cubes . By default, will be an 5x5 wall (unless you change the columns and rows value)
    /// How does it works? It will use a nested if conditioner (And if conditioner insider another if conditioner). 
    /// It will first perform a loop "rows" numbers of times. If your row value is 5, it will perform the initial loop 5 time. 
    /// {
    ///         For each iteration of the first loop, the system will perform a secondary loop "columns" numbers of times. If your column value is 5, this secondary loop will be
    ///         performed 5 times.
    ///         {
    ///         Inside this secondary loop, the system will create a cube, change its position , add the custom script ID, and call its Start() method.
    ///         The position of each cube is calculated by using the counter of both first and secondary loops.
    ///         Example: Counter for firt loop is "i" . Counter of secondary loop is "j"
    ///         Horizontal position of cube is equal to the x coodrinate of the initialPosition + the horizontal scale of the generated cube , all of that multiplied 
    ///         by the counter of the first loop. cube pos x = (InitialPosition.x  + cube.transform.localScale.x) * i // 
    ///         Vertical position of cube is equal to the y coodrinate of the initialPosition - the vertical scale of the generated cube , all of that multiplied 
    ///         by the counter of the secondary loop. cube pos y = (InitialPosition.y  - cube.transform.localScale.y) * j // 
    ///         }
    ///  }
    ///   
    /// This will create a wall that goes from left to right and from top to bottom. 
    /// If you want to create walls with differentorientations, you will need to change the x, y and z components of the vector "currentPos" as needed.
    /// 
    /// </summary>
    void CreateWalls()
    {
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                
                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube); // // create a cube game object
                cube.AddComponent<ID>(); cube.GetComponent<ID>().Start(); //Add the custom script ID and call its start method.
                //Modify the position of the cube.
                currentPos = new Vector3((InitialPosition.x  + cube.transform.localScale.x)*i, (InitialPosition.y - cube.transform.localScale.y)*j, InitialPosition.z);
                cube.transform.position = currentPos;
            }
        }
    }

    /// <summary>
    /// CreateWalls_Dimmensions 
    /// This method is very similar to the previous one with a very important difference: Instead of telling the system how many cubes do we want to place on each axis, 
    /// we will give the desired dimmensions of our wall (how long and tall you want the wall to be), and also we wil ltell the system how big do we want each of the cubes to be.
    /// With that input information, the system will calculate the ammount of cubes needed to fill up the dimmensions of the wall
    /// # of columns can be calculated dividing width of the wall by the desired scale. 
    /// Example: If you want a wall to be 10 units long  and each of the cubes to be 2 units long, you will need 5 cubes to fill up the dimmesion.
    /// # columns = width / scale .  # columns = 10 / 2 = 5;
    ///  # of rows can be calculated dividing height of the wall by the desired scale. 
    /// Example: If you want a wall to be 20 units tall  and each of the cubes to be 5 units tall, you will need 4 cubes to fill up the dimmesion.
    /// # rows = height / scale .  # rows = 20 / 5 = 4;
    /// </summary>
    void CreateWalls_Dimmensions()
    {
        // number of cubes = lenght / scale x of cubes
        columns = width / scale;
        rows = height / scale;
        for (int i = 0; i < rows; i++) // start of the first loop. How many cubes to place at height of the wall
        {
            for (int j = 0; j < columns; j++) //Start of the secondary loop. How many cubes to place at lenght of the wall
            {

                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube); // create a cube game object
                cube.AddComponent<ID>(); cube.GetComponent<ID>().Start(); // Add the custom script ID and call its start method.
                cube.transform.localScale = cube.transform.localScale * scale; // Modify the scale of the cube with our desired scale.
                currentPos = new Vector3((InitialPosition.x + cube.transform.localScale.x) * i, (InitialPosition.y - cube.transform.localScale.y) * j, InitialPosition.z); 
                // Modifiy the position of the cube. 
                cube.transform.position = currentPos;
            }
        }
    }


    /// <summary>
    /// Using the same approach, we could aslo generate floors made of cubes.
    /// The different lies on the modification of the x and  z  component of the vector current position (instead of x and y on the previous examples).
    /// </summary>
    void CreateFloor_Dimmensions()
    {
        // number of cubes = lenght / scale x of cubes
        columns = width / scale;
        rows = height / scale;
        for (int i = 0; i < rows; i++) // start of the first loop. How many cubes to place at height of the wall
        {
            for (int j = 0; j < columns; j++) //Start of the secondary loop. How many cubes to place at lenght of the wall
            {

                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube); // create a cube game object
                cube.AddComponent<ID>(); cube.GetComponent<ID>().Start(); // Add the custom script ID and call its start method.
                cube.transform.localScale = cube.transform.localScale * scale; // Modify the scale of the cube with our desired scale.
                currentPos = new Vector3((InitialPosition.x + cube.transform.localScale.x) * i, InitialPosition.y, (InitialPosition.z - cube.transform.localScale.z) * j);
                // Modifiy the position of the cube. 
                cube.transform.position = currentPos;
            }
        }
    }


    void RandomFloor()
    {
        width = Random.Range(5, 50);
        height = Random.Range(5, 50);
        scale = Random.Range(0.5f, 5);
        // number of cubes = lenght / scale x of cubes
        columns = width / scale;
        rows = height / scale;
        for (int i = 0; i < rows; i++) // start of the first loop. How many cubes to place at height of the wall
        {
            for (int j = 0; j < columns; j++) //Start of the secondary loop. How many cubes to place at lenght of the wall
            {

                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube); // create a cube game object
                cube.AddComponent<ID>(); cube.GetComponent<ID>().Start(); // Add the custom script ID and call its start method.
                cube.transform.localScale = cube.transform.localScale * scale; // Modify the scale of the cube with our desired scale.
                currentPos = new Vector3((InitialPosition.x + cube.transform.localScale.x) * i, InitialPosition.y, (InitialPosition.z - cube.transform.localScale.z) * j);
                // Modifiy the position of the cube. 
                cube.transform.position = currentPos;
            }
        }
    }

    void GenerateFloor()
    {
        // width = Random.Range(5, 50);
        // height = Random.Range(5, 50);
        scale = 4;
        // number of cubes = lenght / scale x of cubes
        columns = width / scale;
        rows = height / scale;
        for (int i = 0; i < rows; i++) // start of the first loop. How many cubes to place at height of the wall
        {
            for (int j = 0; j < columns; j++) //Start of the secondary loop. How many cubes to place at lenght of the wall
            {
                print(InitialPosition);
                
                GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube); // create a cube game object
                cube.tag = "Environment";
                cube.AddComponent<PlatformTouched>();
                cube.GetComponent<PlatformTouched>().token = this.token;
                cube.GetComponent<Renderer>().material.color = new Color32(255, 255, 255, 133);
                cube.AddComponent<ID>(); cube.GetComponent<ID>().Start(); // Add the custom script ID and call its start method.
                // https://docs.unity3d.com/ScriptReference/Transform-localScale.html
                cube.transform.localScale = new Vector3(scale,1,scale); // Modify the scale of the cube with our desired scale.

                currentPos = new Vector3(InitialPosition.x + (cube.transform.localScale.x * i), InitialPosition.y, (InitialPosition.z - (cube.transform.localScale.z * j)));
                // Modifiy the position of the cube. 
                cube.transform.position = currentPos;
            }
        }
    }

}
