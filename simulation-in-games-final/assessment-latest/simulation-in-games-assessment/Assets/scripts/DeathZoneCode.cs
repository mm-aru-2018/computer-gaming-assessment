﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathZoneCode : MonoBehaviour {

    public GameObject player, spawnVolume,cylinder, isGameOver;
    public ObjectMovement objectMovement;
    public Scoring scoring;
    public bool playerDead = false;
    public bool gameOver, livesIncremented;
    public int lives;
    void Awake()
    {
        isGameOver = GameObject.Find("GameOver");
        isGameOver.SetActive(false);
    }
	// Use this for initialization
	void Start () {
        scoring = GameObject.Find("GameScoring").GetComponent<Scoring>();
        player = GameObject.FindGameObjectWithTag("Player");
        objectMovement = player.GetComponentInChildren<ObjectMovement>(); // if player reaches final platform and has no lives left, do not invoke game over
        cylinder = GameObject.Find("Cylinder");
        spawnVolume = GameObject.Find("SpawnArea");
        gameOver = false;
        if (player != null)
        {
            Debug.Log("Player found!");
        }
        lives = 3;
	}
	
	// Update is called once per frame
	void Update () {
		if (playerDead)
        {
            cylinder.transform.position = spawnVolume.transform.position;
            lives--;
            Debug.Log("You have " + lives + " lives remaining.");
            playerDead = false;
        }
        // if player reaches final platform and has no lives left, do not invoke game over
        if (lives == 0 && !objectMovement.gameFinished)
        {
            Destroy(player);
            gameOver = true;
            Cursor.visible = true; // re-enable cursor to allow interaction with game over prompt
        }

        GameOver();

    }
    void IncrementLives()
    {
        
        if (!livesIncremented)
        {
            
            lives++;
            livesIncremented = true;
        }
    }
    void GameOver()
    {
        if (gameOver)
        {
            isGameOver.SetActive(true);
            // Debug.Log("Game over!");
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && player != null)
        {
            playerDead = true;
        }
    }
    void OnTriggerExit(Collider other)
    {

    }
}
