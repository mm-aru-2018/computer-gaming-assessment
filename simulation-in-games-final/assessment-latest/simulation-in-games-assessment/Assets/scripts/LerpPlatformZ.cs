﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpPlatformZ : MonoBehaviour {
    float timer;
    public float magnitude = 2f;
    public float speed = 1f;
    public Vector3 velocity;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        // time since level load prevents platform offsetting, as not based on when program starts, unlike Time.time.
        velocity.z = (Mathf.Cos(Time.timeSinceLevelLoad * speed) * magnitude);


        this.transform.position += velocity * Time.deltaTime;
    }
}
