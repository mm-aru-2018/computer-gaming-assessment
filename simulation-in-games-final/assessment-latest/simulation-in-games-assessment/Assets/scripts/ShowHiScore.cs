﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowHiScore : MonoBehaviour {
    Text text;
    FinalScore finalScore;

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
        finalScore = GameObject.Find("FinalPlatform").GetComponent<FinalScore>();
	}
	
	// Update is called once per frame
	void Update () {
        text.text = ("High score: " + finalScore.score.ToString());
        if (LoadSaveScore.lss.highScore > finalScore.score)
        {
            text.text = ("High score: " + LoadSaveScore.lss.highScore.ToString());
        }
	}
}
