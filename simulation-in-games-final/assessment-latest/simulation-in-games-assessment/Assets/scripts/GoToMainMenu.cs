﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // included to allow the loading of different scenes
public class GoToMainMenu : MonoBehaviour {

    [SerializeField]
    string _sceneName;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GoToMain()
    {
        SceneManager.LoadScene(_sceneName);
        Time.timeScale = 1f;
    }
}
