﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowLives : MonoBehaviour {
    Text text;
    DeathZoneCode deathZoneCode;
    // Use this for initialization
    void Start () {
        text = GetComponent<Text>();
        deathZoneCode = GameObject.Find("DeathZone").GetComponent<DeathZoneCode>();
    }
	
	// Update is called once per frame
	void Update () {
        text.text = ("Lives: " + deathZoneCode.lives.ToString());
	}
}
