﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformTouched : MonoBehaviour {
    public bool platformTouched;
    public int score;
    FindPlatforms findPlatforms;
    public GameObject token;
    RandFriction randFriction;
    Scoring scoring;
    public bool tokenSpawned;
    bool changePlatformColor = false;
    // public int score;
	// Use this for initialization
	void Start () {
        // score = 0;
        platformTouched = false;
        scoring = GameObject.Find("GameScoring").GetComponent<Scoring>();
        findPlatforms = GameObject.Find("FindPlatforms").GetComponent<FindPlatforms>();
        tokenSpawned = false;

	}
	
    GameObject GetToken()
    {
        return token;
    }

	// Update is called once per frame
	void Update () {
		if (platformTouched)
        {
            

            // increment player's score
            scoring.score += 30;
            
            // disable to prevent additional scoring by simply jumping on the same platform
            this.enabled = false;
        }
        if (this.gameObject.layer == 8 && this.gameObject.name != "FinalPlatform" && !changePlatformColor)
        {
            this.GetComponent<Renderer>().material.color = new Color32(0, 171, 255, 133);
            changePlatformColor = true;
        }
	}
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player" && this.enabled && this.gameObject.layer != 12)
        {
            // move spawn point here
            findPlatforms.spawnArea.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + 1, this.gameObject.transform.position.z);
            platformTouched = true;

            // show player that platform has been touched
            // this.GetComponent<Renderer>().material.color = new Color32(0, 0, 0, 133);

            print(platformTouched);
        }
        else if (collision.gameObject.tag == "Player" && this.enabled)
        {
            // move spawn point here
            findPlatforms.spawnArea.transform.position = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y + 1, this.gameObject.transform.position.z);
            // platformTouched = true;

            // print(platformTouched);
        }
        if (collision.gameObject.tag == "Pellet")
        {
            // decrement friction
            randFriction = this.gameObject.GetComponent<RandFriction>();
            if (randFriction != null)
            {
                randFriction.randFriction -= 0.1f;
                if (randFriction.randFriction <= 0f)
                {
                    
                    // remove reduced jumpforce constraint by changing layer
                    this.gameObject.layer = 8;

                    // spawn token
                    if (!tokenSpawned)
                    {
                        // spawn token
                        Object.Instantiate(GetToken(), this.transform.position + 1*(Vector3.up), this.transform.rotation);

                        // prevent additional tokens from being spawned
                        tokenSpawned = true;
                        
                    }

                }
                
            } else if (randFriction == null)
            {
                Debug.Log("No friction");
             
            }

            Destroy(collision.gameObject);
        }
    }
}
